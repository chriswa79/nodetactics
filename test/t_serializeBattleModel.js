var expect = require("chai").expect;

const _ = require('lodash')
const SampleBattleBuilder = require('app/common/battle/SampleBattleBuilder')
const BattleModel = require('app/common/model/BattleModel')
const BattleUnitModel = require('app/common/model/BattleUnitModel')


describe("SerializableModel", function() {
    describe("thaw/freeze BattleUnitModel", function() {
        it("identity", function() {
            var battleModelDoc = SampleBattleBuilder('asdf1234')
            var battleUnitModelDoc = battleModelDoc.units.alice

            var battleUnitModel = new BattleUnitModel(undefined, undefined, battleUnitModelDoc)

            var battleUnitModelDoc2 = battleUnitModel.freeze()

            expect(battleUnitModelDoc2).to.deep.equal(battleUnitModelDoc)

            //done()
        });
    });
    describe("thaw/freeze BattleModel", function() {
        it("identity", function() {
            var battleModelDoc = SampleBattleBuilder('asdf1234')

            var battleModel = new BattleModel(battleModelDoc)

            var battleModelDoc2 = battleModel.freeze()

            expect(battleModelDoc2).to.deep.equal(battleModelDoc)

            //done()
        });
    });
});

