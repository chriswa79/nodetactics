var expect = require("chai").expect;
var { newRoot, DisplayNode } = require("../src/client/DisplayTree");
var { p, rect } = require("../src/common/geom");

function sampleStructure() {

    var ids = {}
    function setId(node, id) {
        node.id = id
        ids[id] = node
        return node
    }

    var root = setId(newRoot(), 'root')

    root.appendChild(setId(new DisplayNode(), '1'))
    root.appendChild(setId(new DisplayNode(), '2'))
    root.appendChild(setId(new DisplayNode(), '4'))
    root.insertBefore(setId(new DisplayNode(), '3'), ids['4'])

    ids['2'].appendChild(setId(new DisplayNode(), '2.1'))
    ids['2'].appendChild(setId(new DisplayNode(), '2.2'))

    return { root, ids }
}

describe("DisplayTree", function() {
    describe("DisplayNodes", function() {
        it("inserting and appending children", function() {
            var { root, ids } = sampleStructure()

            var idSeq = []
            root.forEach(node => idSeq.push(node.id))
            expect(idSeq.join(' ')).to.equal("root 1 2 2.1 2.2 3 4")

            //done()
        });
        it("offsets are preserved", function() {
            var { root, ids } = sampleStructure()

            ids['1'].offset = p(50, 50)
            ids['2'].offset = p(100, 100)
            ids['2'].rect = rect(p(100, 100))
            ids['2.1'].offset = p(10, 10)
            ids['2.1'].rect = rect(p(0, 0), p(10, 10))
            ids['2.2'].rect = rect(p(0, 0), p(10, 10))

            expect( root.pickFirst( p(115, 115) ).id ).to.equal('2.1')
            expect( root.pickFirst( p(105, 105) ).id ).to.equal('2.2')
            expect( root.pickFirst( p(105, 115) ).id ).to.equal('2')

            //done()
        });
    });
});
