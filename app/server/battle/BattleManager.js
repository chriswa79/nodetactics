const BattleServer = require('./BattleServer')

class BattleManager {
    constructor() {
        this.activeBattles = {}
    }
    connectUserToBattle(user, battleDoc) {
        // check if the battle is already active (e.g. multiplayer opponent or disconnection delayed)
        var activeBattle = this.activeBattles[ battleDoc._id ]
        // ... if not, start it
        if (!activeBattle) {
            activeBattle = new BattleServer(battleDoc)
            this.activeBattles[ battleDoc._id ] = activeBattle
        }
        // connect the user to the battle
        activeBattle.connectUser(user)
    }
}

module.exports = new BattleManager()
