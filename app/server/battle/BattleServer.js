const BattleModel = require('../../common/battle/model/BattleModel')
const BattleSimulator = require('./BattleSimulator')
const AI = require('./ai/AI')

class BattleServer {
    constructor(battleDoc) {
        this.model = new BattleModel(battleDoc)
        this.simulator = new BattleSimulator(this.model) // BattleSimulator will register itself as model.sim
        this.model.turn.advanceTurnIfNecessary() // advance until the first unit's first turn!
        this.connectedUsers = {}
    }
    connectUser(user) {
        var battleDoc = this.model.freeze()
        var userTeam = _.find(this.model.teams, team => team.userKey === user.userKey)
        if (!userTeam) { return console.error(`user (userKey ${user.userKey}) not found in battle ${this.model._id}`) }
        this.connectedUsers[user.userKey] = user
        user.teamId = userTeam.teamId
        user.send('battle.start', { battleDoc, myTeamId: userTeam.teamId })
        user.on('message', msg => this.handleUserMessage(user, msg))

        // wait until at least one user has connected before running any AI turns
        this.advanceTimeIfNecessary()
    }
    shutdown() {
        // TODO
    }
    handleUserMessage(user, msg) {
        var [ messageType, messageDetails ] = msg
        if (messageType === 'battle.decision') {
            var unit = this.model.getActiveUnit()

            if (user.teamId !== unit.teamId) {
                user.send('hud.toast', "Not your unit to control!")
                return
            }

            var decisionDoc = messageDetails
            var ability = unit.getAbility(decisionDoc.abilityIndex)

            this.executeAbility(ability, decisionDoc.target)
        }
    }
    executeAbility(ability, target) {
        //this.model.decisionCount += 1
        //ability.doc.decisionId = this.model.lastDecisionId
        //DB.battleLogDecisions.create(undefined, Object.assign({ battleId: this.battleId }, unit.unitId, ))

        if (ability.isPossible() && ability.isTargetValid(target)) {
            ability.execute(target)
        }

        this.advanceTimeIfNecessary()

    }
    advanceTimeIfNecessary() {
        this.model.turn.advanceTurnIfNecessary()

        // play out any AI turns
        while (!this.model.winner) {
            var activeUnit = this.model.getActiveUnit()
            var activeTeam = activeUnit && this.model.teams[ activeUnit.teamId ]
            if (activeTeam && activeTeam.type === 'ai') {
                var ai = new AI(this.model)
                ai.execute()
                this.model.turn.advanceTurn()
            }
            else {
                break
            }
        }

        var recordedResults = this.simulator.getRecordedResults()
        if (recordedResults.length) {
            _.each(this.connectedUsers, user => {
                user.send('battle.results', recordedResults)
            })
        }

        if (this.model.winner) {
            this.shutdown()
        }
    }
}

module.exports = BattleServer
