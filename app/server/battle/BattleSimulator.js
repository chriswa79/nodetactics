const BattleResultApplier = require('app/common/battle/BattleResultApplier')

class BattleSimulator {
    constructor(model) {
        this.model = model
        this.model.sim = this
        this.resultApplier = new BattleResultApplier(this.model)
        this.recordedResults = []

        this.now = 0
        var activeUnit = this.model.getActiveUnit()
        if (activeUnit) {
            this.now = activeUnit.nextTurn
        }

        if (global.DEBUG) { this._updateModelForModificationAssertions() }
    }

    getRecordedResults() {
        var results = this.recordedResults
        this.recordedResults = []
        return results
    }

    result(resultDoc) {
        if (global.DEBUG) { this._assertModelIsUnmodified() }

        this.recordedResults.push(_.cloneDeep(resultDoc))
        this.resultApplier.applyResult(resultDoc)

        if (global.DEBUG) { this._updateModelForModificationAssertions() }
    }

    _updateModelForModificationAssertions() {
        this._modelSnapshot = JSON.stringify(this.model.freeze())
    }
    _assertModelIsUnmodified() {
        var currentModelSnapshot = JSON.stringify(this.model.freeze())
        if (currentModelSnapshot !== this._modelSnapshot) {
            console.error(`!!! model has been changed outside of BattleResultApplier.applyResult!`)
        }
    }
}

module.exports = BattleSimulator
