const { p } = require('app/common/geom')
const Dijkstra = require('app/common/Dijkstra')

class AI {
    constructor(model) {
        this.model = model
        this.activeUnit = this.model.getActiveUnit()
        this.dijkstra = new Dijkstra(this.model.field.grid, this.activeUnit.coords, this.model.isCoordsWalkable.bind(this.model))
    }
    execute() {
        var enemyAdjacentSquares = this.scoreEnemyAdjacentSquares()

        var immediateOptions = _.filter(enemyAdjacentSquares, score => score.dist <= this.model.turn.movesLeft)
        if (immediateOptions.length) {
            var chosenOption = immediateOptions[ Math.floor(immediateOptions.length * Math.random()) ]
            this.moveAndMeleeAttack(chosenOption)
            return
        }

        var closestAdjacentSquare = _.sortBy(enemyAdjacentSquares, ['dist'])[0]
        if (closestAdjacentSquare) {
            this.moveToward(closestAdjacentSquare)
        }
    }
    moveAndMeleeAttack(enemyAdjacentSquare) {
        var meleeCoords = enemyAdjacentSquare.enemy.coords.clone() // just in case the enemy moves?!
        this.moveToward(enemyAdjacentSquare)
        var melee = this.activeUnit.getAbility(0) // melee
        melee.execute(meleeCoords)
    }
    moveToward(enemyAdjacentSquare) {
        var path = this.dijkstra.findAppealingPath(enemyAdjacentSquare.coords)
        var furthestPossibleCoords = path.slice(0, this.model.turn.movesLeft).slice(-1)[0]
        if (furthestPossibleCoords) {
            var move = this.activeUnit.getAbility('move')
            move.execute(furthestPossibleCoords)
        }
    }
    scoreEnemyAdjacentSquares() {
        var enemyAdjacentSquares = []
        var enemies = this.getEnemyUnits(this.activeUnit.teamId)
        _.each(enemies, enemy => {
            _.each(Dijkstra.cardinalDirections, dir => {
                var coords = enemy.coords.plus(p.fromArray(dir))
                var scoreCell = this.dijkstra.resultGrid.getCell(coords)
                if (scoreCell) {
                    //scoreCell.target = enemy
                    enemyAdjacentSquares.push({ coords, enemy, dist: scoreCell.result })
                }
            })
        })
        return enemyAdjacentSquares
        //return scoreGrid
    }
    getEnemyUnits(myTeamId) {
        return _.filter(this.model.units, unit => unit.teamId !== myTeamId)
    }
}

module.exports = AI
