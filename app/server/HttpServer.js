const config = {
    port: 8000
}

const http      = require('http')
const fs        = require('fs')
const path      = require('path')

// http server
const httpServer = http.createServer((request, response) => {
    serveStaticFile(request.url, response)
})
httpServer.listen(config.port, () => {
    console.log(`http up @ http://localhost:${config.port}/`)
})

module.exports = httpServer

function serveStaticFile(url, httpResponse) {

    // default route is to www/
    var filePath = './www' + url

    // if the url start with "amd/", route to amd/ instead of www/
    if (/^\/amd\//.test(url)) { filePath = '.' + url }

    // add 'index.html' if file is not supplied (e.g. url === '/')
    if (/\/$/.test(filePath)) { filePath += 'index.html' }

    var contentTypeByExtension = {
        '.js':   'text/javascript',
        '.css':  'text/css',
        '.json': 'application/json',
        '.png':  'image/png',
        '.jpg':  'image/jpg',
    }
    var contentType = contentTypeByExtension[path.extname(filePath)] || 'text/html'

    //console.log("serveStaticFile: looking for " + filePath)

    fs.readFile(filePath, (error, content) => {
        if (error) {
            if(error.code === 'ENOENT'){
                httpResponse.writeHead(404)
                httpResponse.end('Not found\n')
            }
            else {
                httpResponse.writeHead(500)
                httpResponse.end(`Server error: ${error.code}\n`)
            }
        }
        else {
            httpResponse.writeHead(200, { 'Content-Type': contentType })
            httpResponse.end(content) // , 'utf-8'
        }
    });
}
