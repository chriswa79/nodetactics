var Datastore = require('nedb')

class Table {
    constructor(name) {
        this.name = name
        this.datastore = new Datastore({ filename: 'db/' + this.name, autoload: true })
    }
    _makeCallback(cb) {
        return (err, doc) => {
            if (err) {
                throw err
            }
            else {
                cb && cb(doc)
            }
        }
    }
    load(idOrQuery, cb) {
        var query = (typeof idOrQuery === 'object') ? idOrQuery : { _id: idOrQuery }
        this.datastore.findOne(query, this._makeCallback(cb))
    }
    save(doc, cb) {
        this.datastore.update({ _id: doc._id }, doc, this._makeCallback(cb))
    }
    create(id, doc, cb) {
        if (typeof doc !== 'object') { throw new Error("DB create function signature: (id?, doc, cb?)") }
        if (id) { doc._id = id }
        this.datastore.insert(doc, this._makeCallback(cb))
    }
}

class DB {
    constructor() {
        this.account          = new Table('account')
        this.battle           = new Table('battle')
        this.battleLogInitial = new Table('battleLogInitial')
        this.battleLogUpdates = new Table('battleLogUpdates')
    }
}

global.DB = new DB()

module.exports = global.DB
