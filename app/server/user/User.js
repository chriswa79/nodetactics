const EventEmitter = require('events').EventEmitter
const UserConnection = require('./UserConnection')
const BattleModel = require('../../common/battle/model/BattleModel')
const BattleServer = require('../battle/BattleServer')
const BattleManager = require('../battle/BattleManager')
const SampleBattleBuilder = require('app/common/battle/SampleBattleBuilder')

class User extends EventEmitter {
    constructor(wsConnection) {
        super()
        this.userKey = undefined
        this.network = new UserConnection(wsConnection, this)
    }
    onDisconnect() {
        // pass
    }
    onNetworkMessage(msg) {
        var [eventName, ...methodArgs] = msg

        // is the user attempting to login?
        if (eventName === 'network.syncReq') {
            this.login(methodArgs[0].userKey)
        }
        else {
            this.emit('message', msg)
        }
    }
    login(userKey) {
        this.userKey = userKey
        if (!this.userKey) { return console.error("syncReq did not include a userKey") }

        this.network.send('network.syncRes', {})

        // attempt to load the user's account
        DB.account.load(this.userKey, doc => {
            if (doc) {
                this.joinActiveBattle()
            }
            // user not found, create one
            else {
                DB.account.create(this.userKey, { counter: 1 }, userDoc => {

                    // also create a sample battle
                    var battleDocWithoutId = SampleBattleBuilder(this.userKey).freeze()
                    DB.battle.create(undefined, battleDocWithoutId, battleDoc => {

                        this.joinActiveBattle()

                    })
                })
            }

        });

    }
    joinActiveBattle() {
        DB.battle.load({ 'teams.userKey': this.userKey }, battleDoc => {
            BattleManager.connectUserToBattle(this, battleDoc)
        })
    }
    send(...args) {
        this.network.send(...args)
    }
}

module.exports = User
