class UserConnection {
    constructor(wsConnection, user) {
        this.wsConnection = wsConnection
        this.user = user

        // let User know when the connection is lost
        this.wsConnection.on('close', () => this.user.onDisconnect())

        // let User know when a message is received
        this.wsConnection.on('message', message => {

            // decode JSON
            try {
                if (message.type !== 'utf8') { throw new Error("not utf8?"); }
                var messagePayload = JSON.parse(message.utf8Data)
            } catch (e) {
                console.log("ws error: invalid JSON: ", message.utf8Data)
                return
            }

            console.log("ws << " + message.utf8Data)
            this.user.onNetworkMessage(messagePayload)
        })
    }
    send(...args) {
        // json encode args and send to the browser
        //console.dir(args, {depth: 10})
        var utf8Data = JSON.stringify(args)
        const maxLength = 100 // Infinity
        console.log("ws >> " + utf8Data.substring(0, maxLength) + (utf8Data.length <= maxLength ? '' : `...(${utf8Data.length} characters)`))
        this.wsConnection.sendUTF(utf8Data)
    }
}

module.exports = UserConnection
