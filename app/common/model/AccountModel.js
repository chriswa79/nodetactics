const SerializableModel = require('app/common/Serializable')

class AccountModel extends SerializableModel {
    constructor(doc) {
        super()
        this.thaw(doc)
    }
}

module.exports = AccountModel
