class BattleUpdateModel {
    constructor(doc) {
        this.thaw(doc)
    }
    thaw(doc) {
        this.battleId = doc.battleId
        this.updateId = doc.updateId
        this.actionDoc = doc.actionDoc
    }
    freeze() {
        return {
            battleId: this.battleId,
            updateId: this.updateId,
            actionDoc: this.actionDoc,
        }
    }
}

module.exports = BattleUpdateModel
