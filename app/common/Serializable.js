class SerializableMixin {
    thaw(doc) {
        var rules = this.serializationRules
        var keyThawer = (self, doc, path, key) => {

            var docValue = doc[key]
            if (rules[key]) {
                self[key] = rules[key].thaw.call(this, docValue)
            }
            else if (_.isArray(docValue)) {
                self[key] = []
                arrayThawer(self[key], docValue, path)
            }
            else if (_.isObject(docValue)) {
                self[key] = {}
                objectThawer(self[key], docValue, path)
            }
            else {
                self[key] = docValue
            }
        }
        var objectThawer = (self, doc, path) => {
            for (var key in doc) {
                keyThawer(self, doc, path + '/' + key, key)
            }
        }
        var arrayThawer = (self, doc, path) => {
            for (var key = 0; key < doc.length; key += 1) {
                keyThawer(self, doc, path + '/' + key, key)
            }
        }
        objectThawer(this, doc, '')
    }
    freeze() {
        var rules = this.serializationRules
        var keyFreezer = (self, path, key, doc) => {

            //console.log(this.constructor.name, path, key, rules[key])

            var selfValue = self[key]
            if (rules[key]) {
                doc[key] = rules[key].freeze.call(this, selfValue)
            }
            else if (rules[key] === false) {
                // pass
            }
            else if (_.isFunction(selfValue)) {
                // pass
            }
            else if (_.isArray(selfValue)) {
                doc[key] = arrayFreezer(self[key], path)
            }
            else if (_.isObject(selfValue)) {
                doc[key] = objectFreezer(self[key], path)
            }
            else {
                doc[key] = selfValue
            }
        }
        var objectFreezer = (self, path) => {
            var doc = {}
            for (var key in self) {
                if (self.hasOwnProperty(key)) {
                    keyFreezer(self, path + '/' + key, key, doc)
                }
            }
            return doc
        }
        var arrayFreezer = (self, path) => {
            var doc = []
            for (var key = 0; key < self.length; key += 1) {
                keyFreezer(self, path + '/' + key, key, doc)
            }
            return doc
        }
        return objectFreezer(this, '')
    }
}

var Serializable = {
    makeSerializable(class_, rules) {
        class_.prototype.thaw = SerializableMixin.prototype.thaw
        class_.prototype.freeze = SerializableMixin.prototype.freeze
        class_.prototype.serializationRules = rules
    }
}

module.exports = Serializable
