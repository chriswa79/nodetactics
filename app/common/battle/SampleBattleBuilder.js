const { p } = require('app/common/geom')
const BattleModel = require('app/common/battle/model/BattleModel')
const BattleUnitModel  = require('app/common/battle/model/BattleUnitModel')
const Dijkstra = require('app/common/Dijkstra')

class SampleUnitBuilder {
    constructor(model, teamId, profession, bitmapGroup = 'goblin_red') {
        this.model = model
        this.teamId = teamId
        this.profession = profession
        this.bitmapGroup = bitmapGroup
        this._stats = {
            maxHealth: 100,
            maxMana: 0,
            str: 3,
            int: 3,
            physicalArmour: 2,
            physicalShield: 2,
            moves: 4,
        }
        this._abilities = [ { ability: 'melee' } ]
        this._buffs = {}
    }
    stats(extraStats) {
        this._stats = _.assign(this._stats, extraStats)
        return this
    }
    buffs(buffList) {
        _.each(buffList, buffId => this._buffs[buffId] = {})
        return this
    }
    ability(abilityId, extra = {}) {
        this._abilities.push(_.assign({ ability: abilityId }, extra))
        return this
    }

    randomUnitName(firstNameAttempt = undefined) {
        var names = ("Agmund Amleth Asgeir Bertil Bjarte Borphlum Byggvir Dagur Denkli Diederik Dominic Edgo Egon Einherjar"
        + " Eirik Elof Erland Fenris Fixandu Gjurd Gorla Goxmeor Grendoz Grompl Halvdan Haukur Helheimr Helva Homlur"
        + " Ignaas Ingefred Isak Jervis Kari Klemenz Kormorflo Leif Lodewijk Lorbo Lund Malto Mikko Morta Nestor"
        + " Olander Ormur Ragnvald Remur Sigfinnur Smlorg Somerled Sven Tapani Toivo Torstein Trencha Tryggvi Ull"
        + " Ulrik Urho Valdimar Valgrind Verdl Vihtori Vixja Wendig Wendirgl").split(' ')
        var name = firstNameAttempt
        while (!name || this.model.units[name]) {
            name = names.length ? names.splice(_.random(names.length - 1), 1) : 'ran_outta_names_' + Math.random()
        }
        return name
    }
    commit() {
        var unitId = this.randomUnitName(this.name)
        var coords = findUnoccupiedCoords(this.model, this.teamId)
        if (!coords) {
            this.model.buildError = `Could not find unoccupied coords to place a unit!`;
            console.log(this.model.buildError)
            return undefined
        }
        var unitDoc = {
            teamId: this.teamId,
            bitmapGroup: this.bitmapGroup,
            nextTurn: Math.floor(Math.random() * 100),
            turnTieBreaker: Math.random(),
            alive: true,
            coords: coords.toArray(),
            facing: this.teamId === 1 ? 'ne' : 'sw',
            health: this._stats.maxHealth,
            mana: this._stats.maxMana,
            stats: this._stats,
            abilities: this._abilities,
            buffs: this._buffs,
            name: unitId,
            profession: this.profession,
        }
        var unit = new BattleUnitModel(this.model, unitId, unitDoc)
        this.model.units[unitId] = unit
        return unit
    }
}

// prototype code to generate a sample battle to demonstrate Model works
function array_rand(array) {
    return array[ Math.floor(array.length * Math.random()) ]
}
function sampleCellDoc() {
    var tileIndex = String(array_rand([0, 1, 2, 3, 4, 5, 6, 10, 11, 12, 13, 14, 15, 16, 17, 20, 21, 22, 23]))
    var obstructionIndex = undefined
    if (Math.random() < 0.4) {
        obstructionIndex = String(array_rand([70, 50, 51, 52, 53, 56, 57, 58, 59, 69, 119, 121, 128, 129,        133]))
    }
    return { tileIndex, obstructionIndex }
}
function findUnoccupiedCoords(model, teamId) {
    var x0 = Math.floor(model.field.grid.size / 2)
    var y0 = Math.floor((model.field.grid.size / 6) * (teamId === 1 ? 1 : 5))

    var attemptCounter = 0
    const reasonableNumberOfAttempts = 100
    while (attemptCounter < reasonableNumberOfAttempts) {
        attemptCounter += 1
        var coords = p(x0 + Math.floor(Math.random() * 4 - 2), y0 + Math.floor(Math.random() * 4 - 2))
        if (model.isCoordsWalkable(coords)) { return coords }
    }
    return undefined
}

function SampleBattleBuilder(userKey) {
    while (true) {
        var size = 20
        var cells = _.range(size*size).map(sampleCellDoc)
        var fieldDoc = { size, cells }
        var modelDoc = {
            teams: [],
            turn: {},
            units: {},
            field: fieldDoc,
            defers: {},
            meta: { nextDeferId: 100 },
        }
        var model = new BattleModel(modelDoc)

        model.teams[1] = { teamId: 1, type: 'user', userKey: userKey }
        model.teams[2] = { teamId: 2, type: 'ai' }
        //model.teams[2] = { teamId: 2, type: 'user', userKey: 'player2' }

        new SampleUnitBuilder(model, 1, 'Fighter', 'human_red'   ).stats({ str: 20, physicalArmour: 6, physicalShield: 10 }).commit()
        new SampleUnitBuilder(model, 1, 'Mage',    'human_purple').stats({ maxMana: 100 }).ability('fireball').commit()
        new SampleUnitBuilder(model, 1, 'Archer',  'human_green' ).ability('snipe').ability('poisonShot').commit()
        new SampleUnitBuilder(model, 1, 'Healer',  'human_blue'  ).stats({ maxMana: 100 }).ability('heal').ability('protection').commit()

        new SampleUnitBuilder(model, 2, 'Goblin', 'goblin_red').stats({ maxHealth: 60 }).commit()
        new SampleUnitBuilder(model, 2, 'Goblin', 'goblin_red').stats({ maxHealth: 60 }).commit()
        new SampleUnitBuilder(model, 2, 'Goblin', 'goblin_red').stats({ maxHealth: 60 }).commit()
        new SampleUnitBuilder(model, 2, 'Goblin', 'goblin_red').stats({ maxHealth: 60 }).commit()

        // validate model
        var anyUnit = _.find(model.units, _.stubTrue)
        var dijkstra = new Dijkstra(model.field.grid, anyUnit.coords, model.isCoordsUnobstructed.bind(model))
        var allUnitsCanPath = true
        _.each(model.units, unit => {
            if (dijkstra.getResult(unit.coords) === undefined) {
                allUnitsCanPath = false
            }
        })
        if (allUnitsCanPath && !model.buildError) {
            console.log("SampleBattleBuilder: success!")
            return model
        }
        console.log("SampleBattleBuilder: not allUnitsCanPath, trying again...")
    }
}

/*
        teams: [
            { teamId: 1, type: 'user', userKey: userKey },
            { teamId: 2, type: 'ai' },
        ],
        turn: {
            unitId: 'bob',
            movesLeft: 3,
            actionsLeft: 1,
            facingLeft: 1,
        },
        units: {
            'alice': sampleUnit('red_goblin', 1, 93),
            'bob': sampleUnit('blue_human', 1, 7),
        },
        field: {
            size,
            cells,
        },
        defers: {
            '1': { deferId: '1', when: { time: 10 }, repeat: { count: 3, delay: 10 }, action: [ 'units', 'alice', 'hurt', { total: 1, damageType: 'poison' }]
                , expireAction: [ 'units', 'alice', 'hurt', { total: -3, damageType: 'poison' }]},
            '2': { deferId: '2', when: { time: 15 }, action: [ 'units', 'bob', 'hurt', { total: 1, damageType: 'poison' }]},
        },
        meta: {
            nextDeferId: 100,
        },
 */

module.exports = SampleBattleBuilder
