const BattleFieldModel = require('./BattleFieldModel')
const BattleTurnModel  = require('./BattleTurnModel')
const BattleUnitModel  = require('./BattleUnitModel')
const BattleDefersModel  = require('./BattleDefersModel')
const BattleResultApplier = require('app/common/battle/BattleResultApplier')
const Serializable = require('app/common/Serializable')

// note: server instantiates this using a document from the database, client instantiates from network.rpc_sync

// sample usage:
// var bm = new BattleModel({_id:123,turn:{},units:{234:{}},field:{size:1,cells:[{}]}})
// bm.freeze()

class BattleModel {
    constructor(doc) {
        this.thaw(doc)
        this.sim = undefined
    }

    setMyTeamId(myTeamId) {
        this.myTeamId = myTeamId
    }

    getTime() {
        var activeUnit = this.getActiveUnit()
        return activeUnit ? activeUnit.nextTurn : 0
    }
    getActiveUnit() {
        return this.turn.unitId ? this.units[this.turn.unitId] : undefined
    }
    isItMyTurn() {
        var activeUnit = this.getActiveUnit()
        return activeUnit && activeUnit.teamId === this.myTeamId
    }
    isUnitActiveAndMine(unit) {
        return this.isItMyTurn() && unit === this.getActiveUnit()
    }
    getUnitAtCoords(coords) {
        return _.find(this.units, unit => unit.coords.eq(coords))
    }
    isCoordsWalkable(coords) {
        return this.isCoordsUnobstructed(coords, true)
    }
    isCoordsUnobstructed(coords, unitsAreConsideredBlocking = false) {
        var cell = this.field.grid.getCell(coords)
        if (!cell) { return false }
        if (cell.obstructionIndex) { return false }
        if (unitsAreConsideredBlocking) {
            if (this.getUnitAtCoords(coords)) { return false }
        }
        return true
    }
    getUnitTurnOrder() {
        return _.sortBy(_.filter(this.units, 'alive'), ['nextTurn', 'turnTieBreaker'])
    }
}
BattleResultApplier.registerModule('battle', {
    complete(data) {
        this.model.winner = data.winner
    },
})
Serializable.makeSerializable(BattleModel, {
    'sim': false,
    'field': {
        thaw(doc) {
            return new BattleFieldModel(this, doc)
        },
        freeze(field) {
            return field.freeze()
        },
    },
    'turn': {
        thaw(doc) {
            return new BattleTurnModel(this, doc)
        },
        freeze(turn) {
            return turn.freeze()
        },
    },
    'units': {
        thaw(doc) {
            return _.mapValues(doc, (unitDoc, unitId) => new BattleUnitModel(this, unitId, unitDoc))
        },
        freeze(units) {
            return _.mapValues(units, unit => unit.freeze())
        },
    },
    'defers': {
        thaw(doc) {
            return new BattleDefersModel(this, doc)
        },
        freeze(defers) {
            return defers.freeze()
        },
    },
    'teams': {
        thaw(doc) {
            return _.keyBy(doc, 'teamId')
        },
        freeze(teams) {
            return _.values(teams)
        },
    },
})

module.exports = BattleModel
