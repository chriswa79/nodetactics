const clamp = require('clamp')
var { p } = require('app/common/geom')
const BattleResultApplier = require('app/common/battle/BattleResultApplier')
const AbilityFactory = require('app/common/battle/ability/AbilityFactory')
const Serializable = require('app/common/Serializable')

class BattleUnitModel {
    constructor(battleModel, unitId, doc) {
        this.battleModel = battleModel
        this.unitId = unitId
        this.thaw(doc)
    }
    get cellIndex() {
        return this.battleModel.field.grid.getCellIndex(this.coords)
    }
    getBitmap(animationName, facing) {
        if (!facing) { facing = this.facing }
        return video.getBitmap(this.bitmapGroup + '.' + facing + '.' + animationName)
    }

    getAbility(abilityIndex) {
        var abilityUnitDoc
        if (abilityIndex === 'move' || abilityIndex === 'facing') {
            abilityUnitDoc = { ability: abilityIndex }
        }
        else {
            abilityUnitDoc = this.abilities[abilityIndex]
        }
        return AbilityFactory.create(this.battleModel, this, abilityUnitDoc)
    }

    getStat(statName) {
        var value = this.stats[statName] || 0
        var flat = 0
        var percentage = 100
        _.each(this.buffs, buff => {
            if (buff.stats) {
                flat += buff.stats[statName + '+'] || 0
                percentage += buff.stats[statName + '*'] || 0
            }
        })
        percentage = clamp(percentage, 10, 1000)
        value = (value + flat) * percentage / 100
        return value
    }
    get moves() {
        return this.getStat('moves') // TODO: rewrite callers to call getStat
    }
    getRelativeFacing(fromCoords) {
        var facingCardinal = p.fromFacing(this.facing) // normalized vector (e.g. [1, 0])
        var deltaCoords = this.coords.clone().subtract(fromCoords)
        var dotProduct = deltaCoords.normalize().dotProduct(facingCardinal)
        if (dotProduct <= -0.5) { return 'front' } // edge case 0.5 for forward-facing diagonals favours 'front' over 'side'
        if (dotProduct >   0.5) { return 'back' } // edge case -0.5 for backward-facing diagonals favours 'side' over 'back'
        return 'side'
    }

    dealDamage(targetUnit, { baseDamage, damageType = 'physical', bonusStat = undefined, originCoords = undefined }) {
        originCoords = originCoords ? originCoords : this.coords
        var bonusDamage = bonusStat ? this.getStat(bonusStat) : 0
        var damage = baseDamage + bonusDamage
        targetUnit.takeDamage(this, { damage, damageType, originCoords })
    }
    takeDamage(sourceUnit, { damage, damageType, originCoords }) {
        var relativeFacing = this.getRelativeFacing(originCoords)
        var isBack = relativeFacing === 'back'
        var isFront = relativeFacing === 'front'

        var armourStat = damageType + 'Armour'
        var armourMitigation = !isBack ? this.getStat(armourStat) : 0
        var shieldStat = damageType + 'Shield'
        var shieldMitigation = isFront ? this.getStat(shieldStat) : 0

        var total = damage - armourMitigation - shieldMitigation
        total = Math.max(total, 1)
        this.hurt({ total, damageType })
    }

    hurt(damage) {
        if (this.alive) {
            this.battleModel.sim.result({
                resultType: 'unit.hurt',
                unitId: this.unitId,
                damage
            })
            if (this.health <= 0) {
                this.battleModel.sim.result({
                    resultType: 'unit.destroy',
                    unitId: this.unitId,
                })
            }
        }
    }
    move(target) {
        this.battleModel.sim.result({ resultType: 'unit.move', unitId: this.unitId, target })
    }

    addOrProlongBuff(buffId, when, tickAction) {

        if (when > 0) { // if caller supplies a when number instead of an object, it's a delta-time...
            when = { time: this.battleModel.sim.now + when }
        }
        var existingBuff = this.buffs[buffId]
        if (existingBuff) {
            var defer = existingBuff.deferId ? this.battleModel.defers.get(existingBuff.deferId) : undefined
            if (defer) {
                this.battleModel.defers.update(defer, { when })
            }
            if (existingBuff.stackMax > 1) {
                var newStack = Math.min( existingBuff.stack + 1, existingBuff.stackMax )
                this.battleModel.sim.result({ resultType: 'unit.updateBuff', unitId: this.unitId, updates: { stack: newStack }})
            }
        }
        else {
            var newDefer = { when }
            if (tickAction) {
                newDefer.action = tickAction
            }
            newDefer.expireAction = [ 'units', this.unitId, 'removeBuff', buffId ]
            var buffData = {}
            buffData.deferId = this.battleModel.defers.add(newDefer)
            this.battleModel.sim.result({ resultType: 'unit.addBuff', unitId: this.unitId, buffId, buffData })
        }
    }
    removeBuff(buffId) {
        var buffData = this.buffs[buffId]
        if (buffData && buffData.deferId) {
            var defer = this.battleModel.defers.get(buffData.deferId)
            this.battleModel.defers.remove(defer)
        }
        this.battleModel.sim.result({ resultType: 'unit.removeBuff', unitId: this.unitId, buffId })
    }

}
BattleResultApplier.registerModule('unit', {
    _setUnitFacingTowardTarget(data) {
        data.unit.facing = this.model.field.getFacing( data.targetCell.coords.clone().subtract(data.unit.coords) )
    },
    move(data) {
        this._setUnitFacingTowardTarget(data)
        data.unit.coords = data.targetCell.coords
        this.model.turn.movesLeft -= 1
    },
    hurt(data) {
        data.unit.health -= data.damage.total
    },
    destroy(data) {
        //data.unit.alive = false
        delete( this.model.units[data.unit.unitId] )
    },
    cast(data) {
        this._setUnitFacingTowardTarget(data)
        this.model.turn.actionsLeft = 0
        this.model.turn.movesLeft = 0
    },
    face(data) {
        data.unit.facing = data.target
        this.model.turn.facingLeft = 0
    },
    addBuff(data) {
        data.unit.buffs[ data.buffId ] = data.buffData
    },
    updateBuff(data) {
        _.forEach(data.updates, (value, key) => { data.unit.buffs[ data.buffId ][ key ] = value })
    },
    removeBuff(data) {
        delete( data.unit.buffs[ data.buffId ] )
    },
})
Serializable.makeSerializable(BattleUnitModel, {
    'battleModel': false,
    'unitId': false,
    'coords': {
        thaw(doc) {
            return p(doc[0], doc[1])
        },
        freeze(coords) {
            return [coords.x, coords.y]
        },
    }
})

module.exports = BattleUnitModel
