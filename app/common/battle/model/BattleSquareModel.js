const Serializable = require('app/common/Serializable')

class BattleSquareModel {
    constructor(battleFieldModel, coords, cellIndex, doc) {
        this.battleFieldModel = battleFieldModel
        this.coords = coords
        this.cellIndex = cellIndex
        this.thaw(doc)
    }
}
Serializable.makeSerializable(BattleSquareModel, {
    'battleFieldModel': false,
    'coords': false,
    'cellIndex': false,
})

module.exports = BattleSquareModel
