const BattleResultApplier = require('app/common/battle/BattleResultApplier')
const Serializable = require('app/common/Serializable')

class BattleTurnModel {
    constructor(battleModel, doc) {
        this.battleModel = battleModel
        this.thaw(doc)
    }

    advanceTurnIfNecessary() {

        // check for end-of-battle condition (e.g. victory)
        if (this._endBattleIfNecessary()) { return }

        var activeUnit = this.battleModel.getActiveUnit()
        if (this.facingLeft < 1 || !activeUnit || !activeUnit.alive) {
            this.advanceTurn()
        }
    }

    _endBattleIfNecessary() {
        var winner = 'nobody' // i.e. everyone died!
        _.each(this.battleModel.units, unit => {
            if (unit.alive) {
                if (winner === 'nobody') {
                    winner = unit.teamId
                }
                else {
                    winner = undefined
                }
            }
        })
        if (winner !== undefined) {
            this.battleModel.sim.result({resultType: 'battle.complete', winner})
        }
        return winner !== undefined
    }

    advanceTurn() {
        var defer
        var defersModel = this.battleModel.defers
        var sim = this.battleModel.sim

        var prevUnit = this.battleModel.getActiveUnit()
        while (true) {

            // check for end-of-battle condition (e.g. victory)
            if (this._endBattleIfNecessary()) { return }

            // run all 'after' defers for this unit
            if (prevUnit && prevUnit.alive) {
                defer = defersModel.findNextUnitTurn(prevUnit, 'after')
                if (defersModel.executeAndResolve(defer)) {
                    continue // we may have killed the unit, or there may be another 'after' defer, so start over...
                }
            }

            // if the prevUnit is still alive, advance their nextTurn time
            if (prevUnit && prevUnit.alive) {
                sim.result({ resultType: 'turn.end', unitId: prevUnit.unitId })
            }

            // for future iterations, forget about the prevUnit... (but we still want to re-check for victory conditions every continue)
            prevUnit = undefined

            // find next unit to have a turn
            var nextUnit = this.battleModel.getUnitTurnOrder()[0]

            // run all time-based defers before that unit starts its turn
            defer = defersModel.findNextTimed(nextUnit.nextTurn)
            if (defer) {
                sim.now = defer.when.time
            }
            if (defersModel.executeAndResolve(defer)) {
                continue // we may have killed the unit, so start over...
            }

            sim.now = nextUnit.nextTurn

            // run all 'before' defers for this unit
            defer = defersModel.findNextUnitTurn(nextUnit, 'before')
            if (defersModel.executeAndResolve(defer)) {
                continue // we may have killed the unit, so start over...
            }

            // we're now ready to start the next unit turn. we will not be looping any more
            break
        }

        // remove the before/after 'tapped' flags so that repeating before/after defers will fire again next time
        sim.result({ resultType: 'defer.untapUnit', unitId: nextUnit.unitId })

        // finally, inform consumers that we're waiting for a decision!
        sim.result({ resultType: 'turn.start', unitId: nextUnit.unitId })
    }

}
BattleResultApplier.registerModule('turn', {
    end(data) {
        data.unit.nextTurn += 100
    },
    start(data) {
        this.model.turn.unitId = data.unit.unitId
        this.model.turn.movesLeft = data.unit.getStat('moves')
        this.model.turn.actionsLeft = 1
        this.model.turn.facingLeft = 1
    },
})
Serializable.makeSerializable(BattleTurnModel, {
    'battleModel': false,
})

module.exports = BattleTurnModel
