const BattleResultApplier = require('app/common/battle/BattleResultApplier')

class BattleDefersModel {
    constructor(battleModel, doc) {
        this.battleModel = battleModel
        this.thaw(doc)
    }
    thaw(doc) {
        this.list = doc
    }
    freeze() {
        return this.list
    }

    get(deferId) {
        return this.list[deferId]
    }
    findNextTimed(maxTime) {
        var sortedDefers = _.sortBy(this.list, defer => defer.when.time || Infinity)
        return _.find(sortedDefers, defer => defer.when.time && defer.when.time <= maxTime)
    }
    findNextUnitTurn(unitId, beforeOrAfter) {
        return _.find(this.list, defer => defer.when[beforeOrAfter] === unitId && !defer.when.tapped )
    }

    _runAction(action) {
        var i = 0
        var objectCursor = this
        var methodCursor = this.battleModel
        while (true) { // follow object key path until we find a method
            if (_.isFunction(methodCursor)) { break }
            if (!methodCursor) { break }
            objectCursor = methodCursor
            methodCursor = methodCursor[action[i]]
            i += 1
        }
        if (methodCursor) {
            methodCursor.apply(objectCursor, action.slice(i)) // call the method with the remaining arguments
        }
    }

    add(defer) {
        var deferId = String(this.battleModel.meta.nextDeferId)
        this.battleModel.sim.result({ resultType: 'defer.add', defer })
        return deferId
    }
    update(defer, updates) {
        this.battleModel.sim.result({ resultType: 'defer.update', deferId: defer.deferId, updates })
    }
    remove(defer) {
        this.battleModel.sim.result({ resultType: 'defer.remove', deferId: defer.deferId })
    }
    resolve(defer) {
        if (!defer.repeat || (defer.repeat.hasOwnProperty('count') && defer.repeat.count < 2)) {
            if (defer.expireAction) {
                this._runAction(defer.expireAction)
            }
            this.remove(defer)
        }
        else {
            this.battleModel.sim.result({ resultType: 'defer.advance', deferId: defer.deferId })
        }
    }

    executeAndResolve(defer) {
        if (defer) {
            if (defer.action) {
                this._runAction(defer.action)
            }
            this.resolve(defer)
        }
        return !!defer
    }
}
BattleResultApplier.registerModule('defer', {
    _decorate(data) {
        if (data.hasOwnProperty('deferId')) {
            data.defer = this.model.defers.list[data.deferId]
        }
    },
    add(data) {
        var nextDeferId = String(this.model.meta.nextDeferId)
        this.model.meta.nextDeferId += 1
        data.defer.deferId = nextDeferId
        this.model.defers.list[nextDeferId] = data.defer
    },
    update(data) {
        _.forEach(data.updates, (value, key) => { data.defer[ key ] = value })
    },
    remove(data) {
        delete(this.model.defers.list[data.deferId])
    },
    advance(data) {
        if (data.defer.when.time) {
            data.defer.when.time += data.defer.repeat.delay
        }
        else {
            data.defer.when.tapped = true
        }
        data.defer.repeat.count -= 1
    },
    untapUnit(data) { // this should be done at the start of the unit's turn
        _.forEach(this.model.defers.list, defer => {
            if (defer.when.who === data.unitId) {
                delete defer.when.before
                delete defer.when.after
            }
        })
    },
})

module.exports = BattleDefersModel
