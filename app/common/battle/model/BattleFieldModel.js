const Grid = require('app/common/Grid')
const BattleSquareModel = require('./BattleSquareModel')
const Serializable = require('app/common/Serializable')

class BattleFieldModel {
    constructor(battleModel, doc) {
        this.battleModel = battleModel
        this.thaw(doc)
    }
    thaw(doc) {
        this.grid = new Grid(doc.size, (coords, cellIndex, grid) => new BattleSquareModel(this, coords, cellIndex, doc.cells[cellIndex]))
    }
    freeze() {
        return {
            size: this.grid.size,
            cells: this.grid.cells.map(battleSquareModel => battleSquareModel.freeze())
        }
    }

    getFacing(coords) {
        if (coords.x > Math.abs(coords.y)) { return 'se' }
        if (coords.x < -Math.abs(coords.y)) { return 'nw' }
        if (coords.y < 0) { return 'sw' }
        return 'ne'
    }
}

module.exports = BattleFieldModel
