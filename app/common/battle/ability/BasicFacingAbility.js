const Dijkstra = require('app/common/Dijkstra')
const StandardAbility = require('./StandardAbility')

class BasicFacingAbility extends StandardAbility {
    constructor(...args) {
        super(...args)
    }
    isPossible() {
        return true
    }
    isTargetValid(target) {
        return _.includes(['ne', 'se', 'sw', 'nw'], target)
    }
    execute(target) {
        this.model.sim.result({resultType: 'unit.face', unitId: this.unit.unitId, target })
    }
}

module.exports = BasicFacingAbility

