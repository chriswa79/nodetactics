const StandardAbility = require('./StandardAbility')
const BasicMoveAbility = require('./BasicMoveAbility')
const BasicFacingAbility = require('./BasicFacingAbility')

const AbilityDocDefaults = {
    class_: StandardAbility,
    icon: 'ability.poison',
    targetingUIId: 'aoe',
    minRange: 1,
    maxRange: 1,
    radius: 0,
}

var AbilityDocs = {

    'move': {
        class_: BasicMoveAbility,
        targetingUIId: 'walk',
        icon: 'ability.move',
        name: 'Move',
    },

    'facing': {
        class_: BasicFacingAbility,
        targetingUIId: 'face',
        icon: 'ability.facing',
        name: 'End Turn',
    },

    'melee': {
        targetingUIId: 'unit',
        icon: 'ability.axe',
        name: "Attack",
        desc: "Punch, kick, it's all in the mind...",
        maxRange: 1,
        damage: { baseDamage: 16, damageType: 'physical', bonusStat: 'str' }
    },

    'fireball': {
        icon: 'ability.fireball',
        name: "Fireball",
        desc: "A small, firey explosion",
        minRange: 2,
        maxRange: 3,
        radius: 1,
        damage: { baseDamage: 10, damageType: 'fire', bonusStat: 'int' }
    },

    'poisonShot': {
        targetingUIId: 'unit',
        icon: 'ability.poison',
        name: "Poison Shot",
        desc: "Damage over time",
        minRange: 3,
        maxRange: 4,
        buff: { buffId: 'poison', when: 300 },
    },

    'snipe': {
        targetingUIId: 'unit',
        icon: 'ability.bow',
        name: "Snipe",
        desc: "A long-range attack",
        minRange: 3,
        maxRange: 5,
        damage: { baseDamage: 10, damageType: 'physical', bonusStat: 'dex' }
    },

    'heal': {
        icon: 'ability.heal',
        name: "Heal",
        desc: "Heal units in a small area",
        minRange: 0,
        maxRange: 3,
        radius: 1,
    },

    'protection': {
        icon: 'ability.protect',
        name: "Protection",
        desc: "Protect units in a small area for several turns",
        minRange: 0,
        maxRange: 2,
        radius: 1,
        buff: { buffId: 'protection', when: 300 },
    },
}

// merge in defaults
AbilityDocs = _.mapValues(AbilityDocs, doc => _.assign({}, AbilityDocDefaults, doc))

module.exports = AbilityDocs
