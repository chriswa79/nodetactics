const Dijkstra = require('app/common/Dijkstra')
const { p } = require('app/common/geom')
const StandardAbility = require('./StandardAbility')

class BasicMoveAbility extends StandardAbility {
    constructor(...args) {
        super(...args)
        this.moves = this.model.getActiveUnit() === this.unit ? this.model.turn.movesLeft : this.unit.getStat('moves')
        this.dijkstra = new Dijkstra(this.model.field.grid, this.unit.coords, this.model.isCoordsWalkable.bind(this.model))
    }
    isPossible() {
        return this.moves > 0
    }
    isTargetValid(target) {
        var targetCoords = p.fromObject(target)
        var dist = this.dijkstra.getResult(targetCoords)
        return dist > 0 && dist <= this.moves
    }
    execute(target) {
        var path = this.dijkstra.findAppealingPath(p.fromObject(target))
        var currentUnitCoords = this.unit.coords.clone()
        while (path.length) {
            var nextCoords = path.shift()

            // check for side-effects of calling this.unit.move which should interrupt this decision
            if (!this.unit.coords.eq(currentUnitCoords)) { return } // if we were moved unexpectedly, stop now
            if (!this.model.isCoordsWalkable(nextCoords)) { return } // if an obstacle popped up in our path, stop now
            if (this.model.turn.movesLeft < 1) { return } // if our moves were drained unexpectedly, stop now

            currentUnitCoords = nextCoords
            this.unit.move(nextCoords)
        }
    }
}

module.exports = BasicMoveAbility
