const AbilityDocs = require('./AbilityDocs')

// factory
const AbilityFactory = {
    create(model, unit, unitAbilityDoc) {
        var abilityId = unitAbilityDoc.ability
        var doc = AbilityDocs[abilityId]
        if (!doc) {
            console.error(`BattleAbility not found: ${abilityId}`)
        }
        var class_ = doc.class_
        return new class_(model, unit, doc, unitAbilityDoc)
    }
}

module.exports = AbilityFactory
