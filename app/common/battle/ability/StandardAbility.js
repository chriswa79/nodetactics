const Dijkstra = require('app/common/Dijkstra')
const { p } = require('app/common/geom')

class StandardAbility {
    constructor(model, unit, doc, unitAbilityDoc = {}) {
        this.model = model
        this.unit = unit
        this.doc = doc
        this.unitAbilityDoc = unitAbilityDoc // e.g. level of ability
    }
    getBitmapName() {
        return this.doc.icon
    }
    isPossible() {
        return this.model.isUnitActiveAndMine(this.unit) ? this.model.turn.actionsLeft > 0 : true
    }
    getTooltipText() {
        return `<H>${this.doc.name}</H>${this.doc.desc}`
    }
    getTargetingUIId(onTargetSelected) {
        return this.doc.targetingUIId
    }
    normalizeTargetCoords(target) {
        if (target.hasOwnProperty('x')) { // either Point or object
            return p.fromObject(target)
        }
        else {
            var targetUnit = this.model.units[target] // unitId
            return targetUnit.coords
        }
    }
    isTargetValid(target) {
        var targetCoords = this.normalizeTargetCoords(target)
        var manhattan = this.unit.coords.manhattan(targetCoords)
        return manhattan >= this.doc.minRange && manhattan <= this.doc.maxRange
    }
    execute(target) {
        var targetCoords = this.normalizeTargetCoords(target)

        var effectType = this.doc.effectType ? this.doc.effectType : this.doc.damage ? this.doc.damage.damageType : 'default'
        var effect = { effectType, effectShape: 'blast', size: this.doc.radius }
        this.model.sim.result({ resultType: 'unit.cast', unitId: this.unit.unitId, ability: this.doc.name, target, effect })

        _.forEach(this.model.units, targetUnit => {
            if (targetCoords.manhattan(targetUnit.coords) <= this.doc.radius) {
                this.doc.damage && this.unit.dealDamage(targetUnit, this.doc.damage)
                this.doc.buff   && targetUnit.addOrProlongBuff(this.doc.buff.buffId, this.doc.buff.when, this.doc.buff.tickAction)
            }
        })
    }
}

module.exports = StandardAbility
