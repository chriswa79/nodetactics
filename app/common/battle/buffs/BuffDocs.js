const StandardBuff = require('./StandardBuff')

const BuffDocDefaults = {
    class_: StandardBuff,
    icon: 'ability.poison',
}

var BuffDocs = {

    'protection': {
        display: 'good',
        icon: 'ability.protect',
        name: "Protection",
        desc: "Magical barrier of protection<BR>???",
        stats: {
            'physicalArmour+' : 10,
            'fireArmour+' : 10,
        },
    },

    'poison': {
        display: 'bad',
        icon: 'ability.poison',
        name: "Poisoned!",
        desc: "Slowly draining health<BR>???",
    },

}

// merge in defaults (and key into buffId property)
BuffDocs = _.mapValues(BuffDocs, (buffDoc, buffId) => _.assign({ buffId }, BuffDocDefaults, buffDoc))

module.exports = BuffDocs
