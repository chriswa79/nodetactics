class StandardBuff {
    constructor(unit, doc, details) {
        this.unit = unit
        this.doc = doc
        this.details = details
        this.model = unit.battleModel
    }
    getTooltipText() {
        var expireTime = (this.model.defers.get(this.details.deferId).when.time - this.model.getTime()) / 100
        var tooltipText = `<H><pink>${this.doc.name}</pink></H>${this.doc.desc}<BR><cyan>Expires in <b>${expireTime}</b> turns</cyan>`
        return tooltipText
    }
    getBitmapName() {
        return this.doc.icon
    }
    getSortOrder() {
        return this.doc.display === 'good' ? 0 : this.doc.display === 'bad' ? 1 : -1
    }
    getVisible() {
        return !!this.doc.display
    }
}

module.exports = StandardBuff
