const BuffDocs = require('./BuffDocs')

// factory
const BuffFactory = {
    create(unit, buffId, details) {
        var doc = BuffDocs[buffId]
        if (!doc) {
            console.error(`BattleBuff not found: ${buffId}`)
        }
        var class_ = doc.class_
        return new class_(unit, doc, details)
    }
}

module.exports = BuffFactory
