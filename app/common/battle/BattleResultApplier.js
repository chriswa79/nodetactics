class BattleResultApplier {
    constructor(model) {
        this.model = model
        this.modules = {}
        _.each(BattleResultApplier.registeredModules, (resultModule, resultModuleName) => {
            this.modules[resultModuleName] = resultModule
            resultModule.resultApplier = this
            resultModule.model = this.model
        })
    }
    applyResult(doc) {
        var [resultModuleName, resultMethodName] = doc.resultType.split('.')
        var data = doc
        this._decorate(data)
        var resultModule = this.modules[resultModuleName]
        if (!resultModule) {
            console.error(`!!! No such resultApplier module: ${resultModuleName}`)
            return
        }
        var resultMethod = resultModule[resultMethodName]
        if (!resultMethod) {
            console.warn(`!!! No such resultApplier method: ${doc.resultType}`)
            return
        }
        if (resultModule._decorate) {
            resultModule._decorate(data)
        }

        // call the result method
        resultModule[resultMethodName](data)
    }
    _decorate(data) {
        if (data.hasOwnProperty('unitId')) {
            data.unit = this.model.units[data.unitId]
        }
        if (data.target && data.target.hasOwnProperty('x') && data.target.hasOwnProperty('y')) {
            data.targetCell = this.model.field.grid.getCell(data.target)
        }
    }
}

BattleResultApplier.registeredModules = {}
BattleResultApplier.registerModule = function(resultModuleName, resultModule) {
    BattleResultApplier.registeredModules[resultModuleName] = resultModule
}

module.exports = BattleResultApplier
