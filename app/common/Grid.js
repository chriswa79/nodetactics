var p = require('./geom').p

class Grid {
    constructor(size, cellConstructor) {
        this.size = size
        this.cells = []
        this.forEachCoords((i, j, cellIndex) => {
            this.cells[cellIndex] = cellConstructor(p(i, j), cellIndex, this)
        })
    }
    forEachCoords(callback) {
        for (var j = 0; j < this.size; j += 1) {
            for (var i = 0; i < this.size; i += 1) {
                var cellIndex = i + j * this.size
                callback(i, j, cellIndex)
            }
        }
    }
    forEachCell(callback) {
        this.forEachCoords((i, j, cellIndex) => callback(this.cells[cellIndex]))
    }
    forEachInRenderOrder(callback) {
        var limit  = this.size - 1
        var i      = 0
        var j      = limit
        var iStart = i
        var jStart = limit
        var jTerm  = limit
        while (true) {
            callback(this.cells[ i + j * this.size ])
            i += 1
            j += 1
            if (j > jTerm) {
                jStart -= 1
                if (jStart < 0) {
                    jStart =  0
                    jTerm  -= 1
                    iStart += 1
                    if (iStart > limit) {
                        break
                    }
                }
                i = iStart
                j = jStart
            }
        }
    }
    getCellIndex(coords) {
        if (coords.x < 0 || coords.x >= this.size || coords.y < 0 || coords.y >= this.size) {
            return undefined // bounds check failed
        }
        return coords.x + coords.y * this.size
    }
    getCell(coords) {
        var cellIndex = this.getCellIndex(coords)
        return cellIndex !== undefined ? this.cells[ cellIndex ] : undefined
    }
    // setCell(coords, value) {
    //     var cellIndex = this.getCellIndex(coords)
    //     if (cellIndex) {
    //         this.cells[ cellIndex ] = value
    //     }
    // }


    to2dArray(mapFunction) { // for debugging: use a mapFunction like cell => cell.obstructionIndex ? 'X' : '.'
        var result = []
        for (var j = 0; j < this.size; j += 1) {
            var row = []
            for (var i = 0; i < this.size; i += 1) {
                var cellIndex = i + j * this.size
                row.push(mapFunction(this.cells[cellIndex]))
            }
            result.push(row)
        }
        return result
    }
}

module.exports = Grid
