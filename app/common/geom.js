const lerp = require('lerp')

function Point(x, y) {
    if (!(this instanceof Point)) {
        return new Point(x, y)
    }
    this.x = x || 0
    this.y = y || 0
}
Point.fromArray = function (arr) {
    return new Point(arr[0] || 0, arr[1] || 0)
}
Point.fromObject = function (obj) {
    return new Point(obj.x || 0, obj.y || 0)
}
Point.fromFacing = function (facing) {
    if (facing === 'ne') { return new Point(  0,  1 ) }
    if (facing === 'se') { return new Point(  1,  0 ) }
    if (facing === 'sw') { return new Point(  0, -1 ) }
    if (facing === 'nw') { return new Point( -1,  0 ) }
    throw new Error(`${facing} is not a valid facing`)
}
Point.prototype.plus = function (dx, dy) {
    if (typeof dx === 'object') {
        return new Point(this.x + dx.x, this.y + dx.y)
    }
    else {
        return new Point(this.x + dx, this.y + dy)
    }
}
Point.prototype.add = function (vec) {
    this.x += vec.x
    this.y += vec.y
    return this
}
Point.prototype.subtract = function (vec) {
    this.x -= vec.x
    this.y -= vec.y
    return this
}
Point.prototype.invert = function () {
    this.x *= -1
    this.y *= -1
    return this
}
Point.prototype.round = function () {
    this.x = Math.round(this.x)
    this.y = Math.round(this.y)
    return this
}
Point.prototype.manhattan = function (vec) {
    return Math.abs(vec.x - this.x) + Math.abs(vec.y - this.y)
}
Point.prototype.getMagnitudeSquared = function () {
    return this.x * this.x + this.y * this.y
}
Point.prototype.getMagnitude = function () {
    return Math.sqrt( this.getMagnitudeSquared() )
}
Point.prototype.isMagnitudeGreaterThan = function (scalar) {
    return this.getMagnitudeSquared() > scalar * scalar
}
Point.prototype.eq = function(vec) {
    return (Math.abs(vec.x - this.x) < Number.EPSILON && Math.abs(vec.y - this.y) < Number.EPSILON)
}
Point.prototype.inArray = function(array) {
    for (var other of array) {
        if (this.eq(other)) { return true }
    }
    return false
}
Point.prototype.normalize = function () {
    var magnitude = this.getMagnitude()
    if (magnitude === 0) {
        this.x = 1
        this.y = 0
    } else {
        this.x /= magnitude
        this.y /= magnitude
    }
    return this
}
Point.prototype.dotProduct = function (vec) {
    return this.x * vec.x + this.y * vec.y
}
Point.prototype.clone = function () {
    return new Point(this.x, this.y)
}
Point.prototype.toString = function () {
    return 'x:' + this.x + ', y:' + this.y
}
Point.prototype.toArray = function () {
    return [this.x, this.y]
}
Point.prototype.toObject = function () {
    return {x: this.x, y: this.y}
}

Point.lerp = function(vec1, vec2, t) {
    return new Point( lerp(vec1.x, vec2.x, t), lerp(vec1.y, vec2.y, t) )
}


function Rectangle(topLeft, size, bottomRight) {
    if (!(this instanceof Rectangle)) {
        return new Rectangle(topLeft, size, bottomRight)
    }
    if (!(topLeft instanceof Point)) {
        throw new Error("Rectangle constructor takes Points")
    }
    if (!size && !bottomRight) {
        this.topLeft = new Point(0, 0)
        this.size    = topLeft.clone()
        this._recalcBottomRight()
    }
    else {
        this.topLeft = topLeft.clone()
        if (size) {
            if (!(size instanceof Point)) { throw new Error("Rectangle constructor takes Points") }
            this.size = size.clone()
            this._recalcBottomRight()
        }
        else {
            if (!(size instanceof bottomRight)) { throw new Error("Rectangle constructor takes Points") }
            this.bottomRight = bottomRight.clone()
            this._recalcSize()
        }
    }
}
Rectangle.prototype._recalcBottomRight = function () {
    this.bottomRight = this.topLeft.plus(this.size)
}
Rectangle.prototype._recalcSize = function () {
    this.size = this.bottomRight.clone().subtract(this.topLeft)
}
Rectangle.prototype.clone = function () {
    return new Rectangle(this.topLeft, this.size)
}
Rectangle.prototype.containsPoint = function(p) {
    return (
        this.topLeft.x <= p.x
        && p.x <= this.bottomRight.x
        && this.topLeft.y <= p.y
        && p.y <= this.bottomRight.y
    )
}
Rectangle.prototype.add = function (vec) {
    this.topLeft.add(vec)
    this.bottomRight.add(vec)
    return this
}
Rectangle.prototype.toArray = function() {
    return [ this.topLeft.x, this.topLeft.y, this.size.x, this.size.y ]
}

const geo = {
    p: Point,
    rect: Rectangle,
}

module.exports = geo
