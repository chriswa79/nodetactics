module.exports = function(quantity, sides) {
    var total = 0
    for (var i = 0; i < quantity; i += 1) {
        total += Math.floor(Math.random() * sides) + 1
    }
    return total
}
