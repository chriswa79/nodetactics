const Grid = require('./Grid')
const { p } = require('./geom')

var cardinalDirections = [[1,0],[-1,0],[0,1],[0,-1]]

class Dijkstra {
    constructor(sourceGrid, startCoords, walkableFunction, maxDist = Infinity) {
        this.sourceGrid = sourceGrid
        this.resultGrid = this.build(startCoords, walkableFunction, maxDist)
    }

    getResult(coords) {
        var resultCell = this.resultGrid.getCell(coords)
        return resultCell ? resultCell.result : undefined
    }

    findAppealingPath(targetCoords) {
        var start = this.resultGrid.getCell(this.startCoords)

        var path = []

        var currentCoords = targetCoords
        var current = this.resultGrid.getCell(currentCoords)
        if (!current || !current.result) { return path } // sanity

        var sanityCounter = 0
        while (current !== start) {
            sanityCounter += 1
            if (sanityCounter > 100) { debugger }
            path.unshift(currentCoords)

            var best = {
                coords: undefined,
                dist: Infinity
            }

            for (var [dx, dy] of cardinalDirections) {
                var neighbourCoords = p(dx, dy).add(currentCoords)
                var neighbour = this.resultGrid.getCell(neighbourCoords)
                if (!neighbour || neighbour.result === undefined || neighbour.result >= current.result) {
                    continue
                } // not the right direction, into an obstacle, or off the grid

                var distFromStart = neighbourCoords.clone().subtract(this.startCoords).getMagnitude()
                var distFromTarget = neighbourCoords.clone().subtract(targetCoords).getMagnitude()
                var totalDist = distFromStart + distFromTarget
                if (totalDist < best.dist) {
                    best.dist = totalDist
                    best.coords = neighbourCoords
                }
            }
            currentCoords = best.coords
            current = this.resultGrid.getCell(currentCoords)
        }

        return path
    }

    // m=client.battle.model;"\n"+m.field.grid.dijkstra_simple(m.units.bort.coords,cell=>cell.obstructionIndex).to2dArray(cell=>cell.result || ' ').map(row => row.join(',')).join('\n')
    build(startCoords, walkableFunction, maxDist) {
        this.startCoords = startCoords
        var resultGrid = new Grid(this.sourceGrid.size, coords => {
            return ({ walkable: walkableFunction(coords), result: undefined })
        })
        //console.log(resultGrid.to2dArray(cell=>cell.walkable ? '#' : ':').map(row => row.join(',')))
        var open = []
        var visit = visitCoords => {
            var visitCell = resultGrid.getCell(visitCoords)
            var oldCost = visitCell.result
            var newCost = oldCost + 1
            if (newCost > maxDist) { return }
            for (var [dx, dy] of cardinalDirections) {
                var neighbourCoords = p(dx, dy).add(visitCoords)
                var neighbour = resultGrid.getCell(neighbourCoords)
                if (neighbour && neighbour.walkable) {
                    var targetCost = neighbour.result
                    if (targetCost === undefined) {
                        neighbour.result = newCost
                        open.push(neighbourCoords)
                    }
                }
            }
        }
        resultGrid.getCell(startCoords).result = 0
        open.push(startCoords)
        while (open.length) {
            visit(open.shift())
        }
        return resultGrid
    }
}
Dijkstra.cardinalDirections = cardinalDirections

module.exports = Dijkstra
