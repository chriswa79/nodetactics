const { p } = require('../../common/geom')
const clamp = require('clamp')
const ImageEffects = require('./ImageEffects')

class Bitmap {
    constructor(sourceElement, sourceOffset, size, renderOffset) {
        this.sourceElement = sourceElement
        this.sourceOffset  = sourceOffset
        this.size          = size
        this.renderOffset  = renderOffset ? renderOffset : p(0, 0)
    }
    blit(x, y) {
        video.ctx.drawImage(
            this.sourceElement,                                // source element (img or canvas)
            this.sourceOffset.x, this.sourceOffset.y,          // source offset
            this.size.x, this.size.y,                          // target size
            x - this.renderOffset.x, y - this.renderOffset.y,  // target offset
            this.size.x, this.size.y                           // target size
        )
    }

    effect(methodName, ...args) {
        return ImageEffects[methodName](this, ...args)
    }
    glow(rgb) { return ImageEffects.glow(this, rgb) }
    lighten() { return ImageEffects.lighten(this) }
    darken() { return ImageEffects.darken(this) }

}

ImageEffects.BitmapClass = Bitmap

module.exports = Bitmap
