var EventEmitter = require('events').EventEmitter
var { p }        = require('../../common/geom')

const Bitmap = require('./Bitmap')

var canvas, ctx // brevity

function makeImageLoadingPromise(img) {
    return new Promise((resolve, reject) => {
        if (img.complete) {
            resolve()
        }
        else {
            img.onload = resolve
            img.onerror = reject
        }
    })
}

var imageRegistrationSlicingStrategies = {
    grid(video, img, tokens) {
        var newBitmaps  = {}
        var name        = img.getAttribute('name')
        var gridSize    = p( parseInt(tokens[0]), parseInt(tokens[1]) )
        var imgSize     = p( img.naturalWidth, img.naturalHeight )
        var bitmapIndex = 0
        var tileCount   = p( Math.floor( imgSize.x / gridSize.x ), Math.floor( imgSize.y / gridSize.y ) )
        var aliases     = img.getAttribute('data-alias')  ? img.getAttribute('data-alias').split(' ') : []
        var offset      = img.getAttribute('data-offset') ? p(...img.getAttribute('data-offset').split(' ').map(Number)) : undefined
        for ( var ty = 0; ty < tileCount.y; ty += 1 ) {
            for ( var tx = 0; tx < tileCount.x; tx += 1 ) {
                var bitmap = new Bitmap(img, p( tx*gridSize.x, ty*gridSize.y ), gridSize, offset)
                newBitmaps[`${name}.${bitmapIndex}`] = bitmap
                newBitmaps[`${name}.${aliases[bitmapIndex]}`] = bitmap
                bitmapIndex += 1
            }
        }
        return newBitmaps
    }
}

class VideoSystem extends EventEmitter {
    constructor() {
        super()
        this.canvas  = canvas = document.getElementsByTagName('canvas')[0]
        this.ctx     = ctx    = canvas.getContext('2d')
        this.bitmaps = {}
        canvas.onselectstart = () => false
    }
    init() {
        var images = Array.from( document.getElementsByTagName('img') )
        var imagePromises = images.map( makeImageLoadingPromise )
        Promise.all( imagePromises )
            .then( results => {
                images.forEach( image => {
                    var slicePropTokens = image.getAttribute('data-slice').split(' ')
                    var sliceStratName = slicePropTokens.shift()
                    var slicingStrategy = imageRegistrationSlicingStrategies[ sliceStratName ]
                    if (!slicingStrategy) { throw new Error(`unknown image slicing strategy for slice = ${image.getAttribute('slice')}`) }
                    var newBitmaps = slicingStrategy(this, image, slicePropTokens)
                    _.merge(this.bitmaps, newBitmaps)
                } )
                this.emit('loaded')
            })
    }
    clear() {
        ctx.clearRect( 0, 0, canvas.width, canvas.height )
    }

    getBitmap(key) {
        if (!this.bitmaps[key]) {
            throw new Error(`No such bitmap: ${key}`)
        }
        return this.bitmaps[key]
    }
    getCenter() {
        return p(canvas.width/2, canvas.height/2)
    }

    text(origin, text, colour, size = 10, align = 'left', strokeColour = undefined, lineWidth = 2) {
        ctx.fillStyle = colour
        ctx.font = size + 'px Verdana'
        ctx.textAlign = align

        if (strokeColour) {
            ctx.strokeStyle = strokeColour
            ctx.lineWidth = lineWidth
            ctx.strokeText(text, origin.x, origin.y)
        }

        ctx.fillText(text, origin.x, origin.y)
        ctx.textAlign = "left"
    }

    fill(fillStyle) {
        if (fillStyle) {
            ctx.fillStyle = fillStyle
        }
        ctx.fill()
    }

    fillRoundedRect(topLeft, size, radius, expand, fillStyle) {
        topLeft = topLeft.clone()
        size = size.clone()
        topLeft.x -= expand
        topLeft.y -= expand
        size.x += expand * 2
        size.y += expand * 2

        var r = radius
        var s = size
        var o = topLeft
        ctx.beginPath()
        ctx.moveTo(           o.x + r,       o.y           )
        ctx.lineTo(           o.x + s.x - r, o.y           )
        ctx.quadraticCurveTo( o.x + s.x,     o.y,            o.x + s.x,     o.y + r       )
        ctx.lineTo(           o.x + s.x,     o.y + s.y - r )
        ctx.quadraticCurveTo( o.x + s.x,     o.y + s.y,      o.x + s.x - r, o.y + s.y     )
        ctx.lineTo(           o.x + r,       o.y + s.y     )
        ctx.quadraticCurveTo( o.x,           o.y + s.y,      o.x,           o.y + s.y - r )
        ctx.lineTo(           o.x,           o.y + r       )
        ctx.quadraticCurveTo( o.x,           o.y,            o.x + r,       o.y           )
        ctx.closePath()
        this.fill(fillStyle)
    }

}

module.exports = VideoSystem
