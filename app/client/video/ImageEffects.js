const clamp = require('clamp')
const { p } = require('../../common/geom')

function makeCanvas(bitmap, padding = 0) {
    var canvas = document.createElement('canvas')
    canvas.width = bitmap.size.x + padding * 2
    canvas.height = bitmap.size.y + padding * 2
    var ctx = canvas.getContext('2d')
    return { canvas, ctx }
}

function bitmapFromCanvas(canvas, renderOffset) {
    var newBitmap = new ImageEffects.BitmapClass(canvas, p(0, 0), p(canvas.width, canvas.height), renderOffset)
    return newBitmap
}

function simpleBitmapFilter(bitmap, pixelCallback) {
    var { canvas, ctx } = makeCanvas(bitmap)
    ctx.drawImage(bitmap.sourceElement, bitmap.sourceOffset.x, bitmap.sourceOffset.y, bitmap.size.x, bitmap.size.y, 0, 0, bitmap.size.x, bitmap.size.y)

    var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height)
    var pixels = imageData.data
    for (var i = 0, l = pixels.length; i < l; i += 4) {
        pixelCallback(pixels, i)
    }
    ctx.putImageData(imageData, 0, 0)
    return bitmapFromCanvas(canvas, bitmap.renderOffset)
}

function cache(bitmap, key, maker) {
    var fullKey = '_cachedEffect.' + key
    if (!bitmap[fullKey]) {
        bitmap[fullKey] = maker()
    }
    return bitmap[fullKey]
}


var ImageEffects = {
    inner(bitmap, colour, width) {
        return cache(bitmap, 'inner.' + colour + '.' + width, () => {
            var { canvas, ctx } = makeCanvas(bitmap)
            ctx.drawImage(bitmap.sourceElement, bitmap.sourceOffset.x, bitmap.sourceOffset.y, bitmap.size.x, bitmap.size.y, 0, 0, bitmap.size.x, bitmap.size.y)
            ctx.strokeStyle = colour
            for (var i = 0; i < width; i += 1) {
                ctx.strokeRect(i, i, canvas.width - i * 2, canvas.height - i * 2)
            }
            return bitmapFromCanvas(canvas, bitmap.renderOffset)
        })
    },
    lighten(bitmap) {
        return cache(bitmap, 'lighten', () => {
            return simpleBitmapFilter(bitmap, (pixels, i) => {
                pixels[i + 0] = clamp( pixels[i + 0] * 2, 0, 255 )
                pixels[i + 1] = clamp( pixels[i + 1] * 2, 0, 255 )
                pixels[i + 2] = clamp( pixels[i + 2] * 2, 0, 255 )
            })
        })
    },
    darken(bitmap) {
        return cache(bitmap, 'darken', () => {
            return simpleBitmapFilter(bitmap, (pixels, i) => {
                pixels[i + 0] = Math.floor( pixels[i + 0] / 2 )
                pixels[i + 1] = Math.floor( pixels[i + 1] / 2 )
                pixels[i + 2] = Math.floor( pixels[i + 2] / 2 )
            })
        })
    },
    glow(bitmap, rgb) {
        if (!rgb) { rgb = [255, 255, 255] }
        var cacheKey = '_glow_' + rgb.join('_')
        if (!bitmap[cacheKey]) {
            var glowSize = p(4, 4).add(bitmap.size)

            //var { glowCanvas, glowCtx } = makeCanvas(bitmap, 2)
            var glowCanvas = document.createElement('canvas')
            glowCanvas.width = glowSize.x
            glowCanvas.height = glowSize.y
            var glowCtx = glowCanvas.getContext('2d')
            //glowCtx.drawImage(bitmap.sourceElement, 2, 2)

            for (let [x, y] of [[-1, -2], [1, -2], [2, -1], [2, 1], [1, 2], [-1, 2], [-2, 1], [-2, -1]]) {
                glowCtx.drawImage(
                    bitmap.sourceElement,
                    bitmap.sourceOffset.x,
                    bitmap.sourceOffset.y,
                    bitmap.size.x,
                    bitmap.size.y,
                    x + 2,
                    y + 2,
                    bitmap.size.x,
                    bitmap.size.y
                )
            }

            var imageData = glowCtx.getImageData(0, 0, glowSize.x, glowSize.y)
            var pixels = imageData.data
            for (var i = 0, l = pixels.length; i < l; i += 4) {
                if (pixels[i+3] > 0) {
                    pixels[i]     = rgb[0]
                    pixels[i + 1] = rgb[1]
                    pixels[i + 2] = rgb[2]
                    pixels[i + 3] = 255
                }
            }
            glowCtx.putImageData(imageData, 0, 0)

            glowCtx.drawImage(
                bitmap.sourceElement,
                bitmap.sourceOffset.x,
                bitmap.sourceOffset.y,
                bitmap.size.x,
                bitmap.size.y,
                2,
                2,
                bitmap.size.x,
                bitmap.size.y
            )

            var newRenderOffset = bitmap.renderOffset.plus(2, 2)

            var glowBitmap = new ImageEffects.BitmapClass(glowCanvas, p(0, 0), glowSize, newRenderOffset)
            bitmap[cacheKey] = glowBitmap
        }
        return bitmap[cacheKey]
    },
}

module.exports = ImageEffects
