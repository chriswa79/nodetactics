const { DisplayNode } = require('../DisplayTree')

class BattleTurnClock extends DisplayNode {
    constructor() {
        super()
        this.active = false
    }

    start(secondsRemaining) {
        this.active = true
        this.secondsRemaining = secondsRemaining
    }

    stop() {
        this.active = false
    }

    updateMe(dt) {
        this.secondsRemaining -= dt / 1000
        if (this.secondsRemaining < 0) {
            this.stop()
        }
    }
    renderMe() {
        if (!this.active) { return }

        var cx = 20
        var cy = 20
        var radius = 18
        var startAngle = Math.PI * 2 * -0.25
        var endAngle = Math.PI * 2 * (this.secondsRemaining / 30 - 0.25)

        video.ctx.beginPath()
        video.ctx.fillStyle = 'black'
        video.ctx.arc(cx, cy, 16, 0, Math.PI * 2);
        video.ctx.fill()

        video.ctx.beginPath()
        video.ctx.fillStyle = 'white'
        video.ctx.strokeStyle = 'black'
        video.ctx.lineWidth = 1
        video.ctx.moveTo(cx, cy);
        video.ctx.arc(cx, cy, radius, startAngle, endAngle);
        video.ctx.lineTo(cx, cy);
        video.ctx.fill()
        video.ctx.stroke()
    }
}

module.exports = BattleTurnClock
