const { DisplayNode } = require('../DisplayTree')

module.exports = class extends DisplayNode {
    renderMe() {
        video.ctx.clearRect(0, 0, video.canvas.width, video.canvas.height)
        var gradient = video.ctx.createLinearGradient(0, 0, 0, video.canvas.height)
        gradient.addColorStop(0, "SkyBlue")
        gradient.addColorStop(0.4, "blue")
        gradient.addColorStop(1, "MidnightBlue")
        video.ctx.fillStyle = gradient
        video.ctx.fillRect(0, 0, video.canvas.width, video.canvas.height)
    }
}
