const { DisplayNode } = require('../DisplayTree')
const { p, rect } = require('../../common/geom')
const lerp = require('lerp')
const clamp = require('clamp')
const TargetingUIFactory = require('./ui/target/TargetingUIFactory')
const BattleUI = require('./ui/BattleUI')
const BattleUISelected = require('./ui/BattleUISelected')
const GuiTooltip = require('app/client/gui/GuiTooltip')
const BuffFactory = require('app/common/battle/buffs/BuffFactory')

const maxAbilities = 8 // not including "move" and "facing" actions
const unitPanelHeight = 40

class ActionButton extends DisplayNode {
    constructor(offset, ability, view) {
        super()
        this.offset = offset
        this.ability = ability
        this.view = view
        this.onClick = undefined
        this.rect = rect(p(40, 40))
        if (this.ability) {
            this.bitmapName = this.ability.getBitmapName()
            this.tooltip = new GuiTooltip(200, this.ability.getTooltipText())
        }
    }
    isEnabled() {
        return this.ability ? this.ability.isPossible() : false
    }
    renderMe({mousePos, mousePick}, offset) {
        video.ctx.fillStyle = this.isEnabled() ? 'green' : 'grey'
        if (this.isEnabled() && mousePick === this) {
            video.ctx.fillStyle = '#66ff66'
        }
        video.ctx.fillRect(offset.x, offset.y, this.rect.size.x, this.rect.size.y)

        if (this.bitmapName) {
            var bitmap = video.bitmaps[this.bitmapName]
            if (this === this.view.unitPanel.selectedActionButton) {
                bitmap = bitmap.effect('inner', 'white', 2)
            }
            else if (!this.isEnabled()) {
                bitmap = bitmap.darken()
            }
            else if (mousePick === this) {
                bitmap = bitmap.lighten()
            }
            bitmap.blit(offset.x, offset.y)
        }

        // display tooltip?
        if (mousePick === this && this.tooltip) {
            this.view.setTooltip(this.tooltip)
        }
    }
    input_click(e) {
        this.isEnabled() && this.onClick && this.onClick()
    }
}

class BuffIcon extends DisplayNode {
    constructor(view, offset, buff) {
        super()
        this.view = view
        this.model = view.model
        this.offset = offset.clone()
        this.rect = rect(p(40, 40))
        this.buff = buff

        this.tooltip = new GuiTooltip(200, this.buff.getTooltipText())
        this.bitmapName = this.buff.getBitmapName()
    }
    renderMe({ mousePos, mousePick }, offset) {
        video.bitmaps[this.bitmapName].blit(offset.x, offset.y)

        if (mousePick === this && this.tooltip) {
            this.view.setTooltip(this.tooltip)
        }
    }
}

class BattleUnitPanel extends DisplayNode {
    constructor(model, view) {
        super()
        this.model = model
        this.view = view
        this.rect = rect(p(video.canvas.width, 40)) // XXX: hitbox doesn't resize when canvas does!
        this.clear()
    }
    clear() {
        this.removeAllChildren()
        this.view.setUI( new BattleUI() )
        this.unit = undefined
        this.offset = p(0, video.canvas.height)
    }
    selectUnit(unit) {
        if (!unit) {
            return this.clear()
        }
        this.unit = unit
        this.removeAllChildren()
        this.buildBuffIcons()
        this.buildActionBar()
        this.selectDefaultActionBarButton()
    }
    buildBuffIcons() {
        var buffs = _.map(this.unit.buffs, (buffDetails, buffId) => BuffFactory.create(this.unit, buffId, buffDetails))
        var sortedBuffs = _.sortBy(buffs, buff => buff.getSortOrder())
        var cursor = p(video.canvas.width, -42)
        _.each(sortedBuffs, buff => {
            if (!buff.getVisible()) { return }
            cursor.x -= 42
            if (cursor.x < 0) {
                cursor.x = video.canvas.width - 42
                cursor.y -= 30
            }
            this.appendChild( new BuffIcon(this.view, cursor, buff) )
        })
    }
    buildActionBar() {
        this.actionButtons = []
        this.unitIsActiveAndMine = this.model.isUnitActiveAndMine(this.unit)
        // movement
        this.appendActionButton('move')
        // techniques
        for (var abilityIndex = 0; abilityIndex < maxAbilities; abilityIndex += 1) {
            if (this.unit.abilities.length > abilityIndex) {
                this.appendActionButton(abilityIndex)
            }
            else {
                this.appendSpacerButton()
            }
        }
        // facing
        if (this.unitIsActiveAndMine) {
            this.appendActionButton('facing')
        }
        else {
            this.appendSpacerButton()
        }
    }
    appendActionButton(abilityIndex) {
        var ability = this.unit.getAbility(abilityIndex)
        var actionButton = this._appendButton(ability)
        actionButton.onClick = () => {

            this.selectedActionButton = actionButton

            var onTargetSelected = target => {
                this.unitIsActiveAndMine && this.view.sendDecision( { abilityIndex, target } )
            }

            var targetingUI = TargetingUIFactory.create( ability, onTargetSelected )
            this.view.setUI(targetingUI)
        }
    }
    appendSpacerButton() {
        this._appendButton(undefined)
    }
    _appendButton(ability) {
        var offset = p(240 + this.actionButtons.length * 40, unitPanelHeight - 40)
        var actionButton = this.appendChild(new ActionButton(offset, ability, this.view))
        this.actionButtons.push(actionButton)
        return actionButton
    }
    selectDefaultActionBarButton() {
        // if movement is still available, choose it
        if (this.actionButtons[0].isEnabled()) {
            this.actionButtons[0].onClick()
        }
        else {
            // if there's nothing left to do but end the turn, choose facing (the last button)
            var enabledCount = this.actionButtons.reduce((acc, actionBarButton) => acc + (actionBarButton.isEnabled() ? 1 : 0), 0)
            var i = this.actionButtons.length - 1
            if (enabledCount === 1 && this.actionButtons[i].isEnabled()) {
                this.actionButtons[i].onClick()
            }
            else {
                this.view.setUI( new BattleUISelected(this.unit) )
            }
        }
    }
    update(dt) {
        // animate slideup
        if (this.unit) {
            this.offset.y = lerp(this.offset.y, video.canvas.height - unitPanelHeight, dt * 0.01);
        }
    }
    renderMe({ mousePos, mousePick }, offset) {
        video.ctx.fillStyle = 'black'
        video.ctx.fillRect(offset.x + 0, offset.y + 0, video.canvas.width, unitPanelHeight)

        if (this.unit) {
            video.bitmaps['portrait.' + this.unit.bitmapGroup].blit(offset.x, offset.y - 24)
            video.text(offset.plus(66, 18), this.unit.name, 'white', 16)
            video.text(offset.plus(66, 32), this.unit.profession, 'white', 12)

            this.drawVitalBar(offset.plus(145, 3 + 12*0), p(90, 10), this.unit.health, this.unit.getStat('maxHealth'), '#cc0000', '#330000', '#cc9999')
            if (this.unit.getStat('maxMana')) {
                this.drawVitalBar(offset.plus(145, 3 + 12*1), p(90, 10), this.unit.mana, this.unit.getStat('maxMana'), '#0000bb', '#000033', '#9999bb')
            }
            var unitTime = this.unit.nextTurn - this.model.getTime()
            if (this.unitIsActiveAndMine) { unitTime = 100 }
            this.drawVitalBar(offset.plus(145, 3 + 12*2), p(90, 10), unitTime, 100, '#aa00aa', '#330033', '#aa99aa')
        }
    }
    drawVitalBar(offset, size, currentValue, maxValue, currentColour, backColour, outlineColour) {
        video.ctx.fillStyle = outlineColour
        video.ctx.fillRect(offset.x, offset.y, size.x, size.y)
        video.ctx.fillStyle = backColour
        video.ctx.fillRect(offset.x + 1, offset.y + 1, size.x - 2, size.y - 2)
        var maxHealth = Math.max(currentValue, maxValue)
        video.ctx.fillStyle = currentColour
        var healthNormalized = maxHealth ? clamp(currentValue / maxHealth, 0, 1) : 0
        video.ctx.fillRect(offset.x + 1, offset.y + 1, healthNormalized * size.x - 2, size.y - 2)
        video.text(offset.plus(size.x / 2, 9), currentValue + ' / ' + maxHealth, 'white', 9, 'center')
    }
}

module.exports = BattleUnitPanel
