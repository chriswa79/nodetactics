const WatchingUIBase = require('./_base')

class WatchingUIBattleComplete extends WatchingUIBase {
    constructor(...args) {
        super(...args)
        this.onWatchingComplete = () => {
            client.gui.modal("Battle Complete", JSON.stringify(this.doc))
        }
    }

    watchInit() {
        this.duration = 100
    }

}

module.exports = WatchingUIBattleComplete
