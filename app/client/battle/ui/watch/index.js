const classMap = {
    'battle.complete': require('./battleComplete'),
    'unit.cast': require('./spellcast'),
    'unit.move': require('./move'),
    'unit.hurt': require('./hurt'),
    'unit.addBuff': require('./addBuff'),
    //'unit.updateBuff': require('./updateBuff'),
    //'unit.removeBuff': require('./removeBuff'),
}

const WatchingUI = new class {
    create(model, view, resultDoc, onWatchingComplete) {
        const _class = classMap[ resultDoc.resultType ]
        if (!_class) {
            return undefined
        }
        const watchingUI = new _class(model, view, resultDoc, onWatchingComplete)
        return watchingUI
    }
}

module.exports = WatchingUI
