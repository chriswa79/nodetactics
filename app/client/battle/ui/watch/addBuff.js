const WatchingUIBase = require('./_base')
const BuffFactory = require('app/common/battle/buffs/BuffFactory')

class WatchingUIAddBuff extends WatchingUIBase {
    constructor(...args) {
        super(...args)
    }

    watchInit() {
        this.duration = 800
        this.buff = BuffFactory.create(this.model.units[this.doc.unitId], this.doc.buffId, this.doc.buffData)
    }

    renderSquareOverlay({ mousePos, mousePick, cell, cellOffset }) {
        var unit = this.unitCellIndexLookup[ cell.cellIndex ]
        if (unit && unit.unitId === this.doc.unitId) {
            video.ctx.fillStyle = 'white'
            video.text(cellOffset.plus(0, -48 - this.age / 400 * 20), '+ ' + this.buff.doc.name, 'white', 12, 'center')
        }
    }
}

module.exports = WatchingUIAddBuff
