const WatchingUIBase = require('./_base')

class WatchingUISpellcast extends WatchingUIBase {
    constructor(...args) {
        super(...args)
    }
    watchInit() {
        this.duration = 400
    }
    renderSquareUnit({ mousePos, mousePick, cell, cellOffset }) {
        var unit = this.unitCellIndexLookup[ cell.cellIndex ]
        if (unit && unit.unitId === this.doc.unitId) {
            var bitmap = unit.getBitmap('cast')
            bitmap.blit(cellOffset.x, cellOffset.y)
            video.ctx.fillStyle = 'white'
            video.text(cellOffset.plus(0, -48 - this.age / 400 * 20), '*' + this.doc.ability + '*', 'white', 12, 'center')
        }
        else {
            super.renderSquareUnit({ mousePos, mousePick, cell, cellOffset })
        }
    }
}

module.exports = WatchingUISpellcast
