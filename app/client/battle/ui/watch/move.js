const WatchingUIBase = require('./_base')
const { p } = require('../../../../common/geom')

class WatchingUIMove extends WatchingUIBase {
    constructor(...args) {
        super(...args)
    }

    watchInit() {
        this.duration = 400
        this.targetCell = this.model.field.grid.getCell(p.fromObject(this.doc.target))
        this.facing = this.model.field.getFacing(this.targetCell.coords.clone().subtract(this.unit.coords))
    }

    watchUpdate() {
        this.lerpedCoords = p.lerp( this.unit.coords, this.targetCell.coords, this.t )
        this.view.field.centerOnCoords(this.lerpedCoords, true)
        this.renderCellIndex = this.view.field.realCellCoordsToRenderCellIndex(this.lerpedCoords)
    }
    renderSquareUnit({ mousePos, mousePick, cell, cellOffset }) {
        var unit = this.unitCellIndexLookup[ cell.cellIndex ]
        if (unit && unit !== this.unit) {
            //newCellOffset = p.lerp( cellOffset, cellOffset.clone().add(this.deltaWorldCoords), this.t )
            var bitmap = unit.getBitmap('idle')
            bitmap.blit(cellOffset.x, cellOffset.y)
        }
        if (cell.cellIndex === this.renderCellIndex) {
            unit = this.unit
            var lerpedOffset = this.view.field.cellCoordsToScreenPoint(this.lerpedCoords)

            var animationName = ['walk1', 'idle', 'walk2', 'idle']
            animationName = animationName[ Math.floor(this.age / 100) % animationName.length ]

            var bitmap = unit.getBitmap(animationName, this.facing)
            bitmap.blit(lerpedOffset.x, lerpedOffset.y)
        }
    }
}

module.exports = WatchingUIMove
