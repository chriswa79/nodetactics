const BattleUI = require('../BattleUI')

class WatchingUIBase extends BattleUI {
    constructor(model, view, doc, onWatchingComplete) {
        super()
        this.model = model
        this.view = view
        this.doc = doc
        this.onWatchingComplete = onWatchingComplete

        if (this.doc.unitId) {
            this.unit = this.model.units[this.doc.unitId]
        }

        this.age = 0
    }
    init() {
        super.init()
        this.watchInit()
    }
    updateMe(dt) {
        this.age += dt
        if (this.age > this.duration) {
            this.onWatchingComplete(this.age - this.duration)
        }
        else {
            this.t = this.age / this.duration
            this.watchUpdate()
        }
    }

    watchInit() {
        this.duration = 200
    }
    watchUpdate() {
    }
}

module.exports = WatchingUIBase
