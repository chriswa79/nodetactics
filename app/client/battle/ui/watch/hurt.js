const WatchingUIBase = require('./_base')

class WatchingUISpellcast extends WatchingUIBase {
    constructor(...args) {
        super(...args)
    }

    watchInit() {
        this.duration = 800
    }

    renderSquareOverlay({ mousePos, mousePick, cell, cellOffset }) {
        var unit = this.unitCellIndexLookup[ cell.cellIndex ]
        if (unit && unit.unitId === this.doc.unitId) {
            video.ctx.fillStyle = 'white'
            video.text(cellOffset.plus(0, -48 - this.age / 400 * 20), this.doc.damage.total, 'white', 12, 'center')
        }
    }
}

module.exports = WatchingUISpellcast
