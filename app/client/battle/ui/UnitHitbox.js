const { DisplayNode } = require('../../DisplayTree')
const { p, rect } = require('../../../common/geom')


class UnitHitbox extends DisplayNode {
    constructor(unit, offset, onClick, size = 'BIG', rgb = undefined) {
        super()
        this.unit = unit
        this.offset = offset
        if (size === 'BIG') {
            this.rect = rect(p(-9, -38), p(18, 42))
        }
        else {
            this.rect = rect(p(-7, -24), p(14, 28))
        }
        this.rgb = rgb
        this.onClick = onClick
    }
    input_click(e) {
        this.onClick(this.unit)
        e.stopPropagation()
    }
    renderMe({ mousePos, mousePick }, offset) {
        //video.ctx.strokeStyle = '#ff00ff'
        //var r = this.rect.clone().add(offset)
        //video.ctx.strokeRect(r.topLeft.x, r.topLeft.y, r.size.x, r.size.y)
    }
}

module.exports = UnitHitbox
