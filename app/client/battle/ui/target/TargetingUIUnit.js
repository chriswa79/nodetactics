const TargetingUIBase = require('./TargetingUIBase')

class TargetingUIUnit extends TargetingUIBase {
    constructor(...args) {
        super(...args)
    }
    init() {
        super.init()
        if (this.isActive()) {
            var filter = unit => this.ability.isTargetValid(unit.unitId)
            var onClick = unit => this.onTargetSelected(unit.coords)
            this.createUnitHitboxes(filter, onClick, this.UnitSelectionMode.BIG, [255, 0, 0])
        }
    }
    isCellTargetable(cell) {
        return this.ability.isTargetValid(cell.coords)
    }
    renderSquareTileOverlay({ mousePos, mousePick, cell, cellOffset }) {
        var inRange = this.ability.isTargetValid(cell.coords)
        if (inRange) {
            this.drawCellPath(cellOffset)
        }
    }
}

module.exports = TargetingUIUnit
