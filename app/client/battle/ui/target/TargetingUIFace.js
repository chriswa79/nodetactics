const TargetingUIBase = require('./TargetingUIBase')

class TargetingUIFace extends TargetingUIBase {
    constructor(...args) {
        super(...args)
        this.unitSelectionMode = this.UnitSelectionMode.SMALL
    }
    getFacingFromMousePos(mousePos) {
        var worldDelta = this.view.field.screenPointToWorldSpace( mousePos ).subtract( this.unit.coords )
        return this.model.field.getFacing(worldDelta)
    }
    preRender({ mousePos, mousePick }) {
        this.facing = this.getFacingFromMousePos(mousePos)
    }
    renderSquareUnit({ mousePos, mousePick, cell, cellOffset }) {
        var unit = this.unitCellIndexLookup[ cell.cellIndex ]
        if (unit === this.unit) {
            var bitmap = unit.getBitmap('idle', this.facing)
            bitmap = bitmap.glow()
            bitmap.blit(cellOffset.x, cellOffset.y)
        }
        else {
            super.renderSquareUnit({ mousePos, mousePick, cell, cellOffset })
        }
    }
    input_click(e) {
        var facing = this.getFacingFromMousePos(e.input.mousePos)
        this.onTargetSelected( facing )
    }
}

module.exports = TargetingUIFace
