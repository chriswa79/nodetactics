const TargetingUIBase = require('./TargetingUIBase')
const Dijkstra = require('../../../../common/Dijkstra')

class TargetingUIWalk extends TargetingUIBase {
    constructor(...args) {
        super(...args)
        this.moves = this.ability.moves
        if (this.isActive()) {
            this.unitSelectionMode = this.UnitSelectionMode.SMALL
        }
    }
    init() {
        super.init()
        this.dijkstra = new Dijkstra(this.model.field.grid, this.unit.coords, this.model.isCoordsWalkable.bind(this.model))
    }

    isCellTargetable(cell) {
        if (!cell) { return false }
        var dist = this.dijkstra.getResult(cell.coords)
        return dist > 0 && dist <= this.moves
    }

    preRender({ mousePos, mousePick }) {
        super.preRender({ mousePos, mousePick })
        this.path = this.getPathForCell(this.hoveredCell)
    }

    getPathForCell(cell) {
        if (cell && this.isCellTargetable(cell)) {
            return this.dijkstra.findAppealingPath(cell.coords)
        }
        return []
    }

    renderSquareTileOverlay({ mousePos, mousePick, cell, cellOffset }) {
        if (this.isCellTargetable(cell)) {
            this.drawCellPath(cellOffset, cell.coords.inArray(this.path))
        }
    }

}

module.exports = TargetingUIWalk
