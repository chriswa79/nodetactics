const BattleUISelected = require('../BattleUISelected')
const UnitHitbox = require('../UnitHitbox')

class TargetingUIBase extends BattleUISelected {
    constructor(ability, onTargetSelected) {
        super(ability.unit)
        this.unit = ability.unit
        this.ability = ability
        this.onTargetSelected = onTargetSelected
        this.unitSelectionMode = this.isActive() ? this.UnitSelectionMode.NONE : this.UnitSelectionMode.BIG
    }
    renderSquareUnit({ mousePos, mousePick, cell, cellOffset }) {
        var unit = this.unitCellIndexLookup[ cell.cellIndex ]
        if (unit) {
            var bitmap = unit.getBitmap('idle')
            if (mousePick instanceof UnitHitbox && mousePick.unit === unit) {
                bitmap = bitmap.glow(mousePick.rgb)
            }
            else if (unit === this.unit) {
                bitmap = bitmap.glow()
            }
            bitmap.blit(cellOffset.x, cellOffset.y)
        }
    }
    isCellTargetable(cell) {
        return false
    }
    pickMe() {
        return true
    }
    input_click(e) {
        if (this.isActive()) {
            var hoveredCell = this.view.field.getCellAtScreenPos(e.input.mousePos)
            if (hoveredCell && this.isCellTargetable(hoveredCell)) {
                this.onTargetSelected(hoveredCell.coords.toObject())
            }
        }
        else {
            super.input_click(e)
        }
    }
    isActive() {
        var battleModel = this.unit.battleModel // cheating?
        return battleModel.isUnitActiveAndMine(this.unit)
    }
    drawCellPath(cellOffset, highlighted) {
        var rgb, lineAlpha
        if (this.isActive()) {
            rgb = highlighted ? '100, 180, 255' : '0, 80, 255'
            lineAlpha = highlighted ? 1 : 0.5
        }
        else {
            rgb = highlighted ? '200, 150, 50' : '100, 80, 0'
            lineAlpha = 0.5
        }
        super.drawCellPath(cellOffset, rgb, lineAlpha)
    }
}

module.exports = TargetingUIBase
