const classMap = {
    walk: require('./TargetingUIWalk'),
    aoe: require('./TargetingUIAOE'),
    unit: require('./TargetingUIUnit'),
    face: require('./TargetingUIFace'),
}

const TargetingUIFactory = new class {
    create(ability, onTargetSelected) {

        const _class = classMap[ ability.getTargetingUIId() ]
        if (!_class) {
            throw new Error(`TargetingUI has no class associated with targetingUIId = "${ability.getTargetingUIId()}"`)
        }
        const targetingUI = new _class(ability, onTargetSelected)
        return targetingUI
    }
}

module.exports = TargetingUIFactory
