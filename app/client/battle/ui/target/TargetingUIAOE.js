const TargetingUIBase = require('./TargetingUIBase')

class TargetingUIAOE extends TargetingUIBase {
    constructor(...args) {
        super(...args)
    }
    isCellTargetable(cell) {
        if (!cell) { return false }
        return this.ability.isTargetValid(cell.coords)
    }
    renderSquareTileOverlay({ mousePos, mousePick, cell, cellOffset }) {
        var inRange = this.isCellTargetable(cell)
        var inRadius = false
        if (this.isCellTargetable(this.hoveredCell)) { // valid target
            inRadius = cell.coords.manhattan(this.hoveredCell.coords) <= this.ability.doc.radius
        }
        if (inRadius || inRange) {
            this.drawCellPath(cellOffset, inRadius)
        }
    }
}

module.exports = TargetingUIAOE
