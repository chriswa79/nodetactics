const { DisplayNode } = require('../../DisplayTree')
const { p } = require('../../../common/geom')
const assert = require('assert');

const tileOffset = p(64, 32)

class BattleUI extends DisplayNode {
    constructor() {
        super()
    }
    init() {
        this.unitCellIndexLookup = {}
        _.forEach(this.model.units, unit => {
            if (this.unitCellIndexLookup[ unit.cellIndex ]) {
                console.warn("more than one unit in cellIndex " + unit.cellIndex)
            }
            this.unitCellIndexLookup[ unit.cellIndex ] = unit
        })
    }
    renderMe({ mousePos, mousePick }) {

        // determine hoveredCell
        this.hoveredCell = undefined
        if (mousePick === this && !this.view.field.isDragging) {
            this.hoveredCell = this.view.field.getCellAtScreenPos(mousePos)
        }

        this.preRender({ mousePos, mousePick })

        this.model.field.grid.forEachInRenderOrder( cell => {
            var cellOffset = this.view.field.cellCoordsToScreenPoint(cell.coords)
            this.renderSquare({ mousePos, mousePick, cell, cellOffset })
        } )

        this.postRender({ mousePos, mousePick })

        if (this.titleText) {
            this.renderTitleText(this.titleText)
        }
    }
    renderTitleText(text) {
        video.text(p(video.canvas.width / 2, 64), text, 'white', 24, 'center', 'black', 5)
    }
    renderSquare({ mousePos, mousePick, cell, cellOffset }) {
        this.renderSquareTile({ mousePos, mousePick, cell, cellOffset })
        this.renderSquareTileOverlay({ mousePos, mousePick, cell, cellOffset })
        this.renderSquareObstruction({ mousePos, mousePick, cell, cellOffset })
        this.renderSquareUnit({ mousePos, mousePick, cell, cellOffset })
        this.renderSquareOverlay({ mousePos, mousePick, cell, cellOffset })
    }

    renderSquareTile({ mousePos, mousePick, cell, cellOffset }) {
        video.getBitmap('tiles.' + cell.tileIndex).blit(cellOffset.x, cellOffset.y)
    }
    renderSquareTileOverlay({ mousePos, mousePick, cell, cellOffset }) {
        // pass
    }
    renderSquareObstruction({ mousePos, mousePick, cell, cellOffset }) {
        if (cell.obstructionIndex) {
            video.getBitmap('tiles.' + cell.obstructionIndex).blit(cellOffset.x, cellOffset.y)
        }
    }
    renderSquareUnit({ mousePos, mousePick, cell, cellOffset }) {
        var unit = this.unitCellIndexLookup[ cell.cellIndex ]
        if (unit) {
            var bitmap = unit.getBitmap('idle')
            bitmap.blit(cellOffset.x, cellOffset.y)
        }
    }
    renderSquareOverlay({ mousePos, mousePick, cell, cellOffset }) {
        // pass
    }
    preRender({ mousePos, mousePick }) {
    }
    postRender({ mousePos, mousePick }) {
    }

    drawCellPath(cellOffset, rgb, lineAlpha) {
        video.ctx.beginPath()
        video.ctx.moveTo( cellOffset.x                  , cellOffset.y - tileOffset.y/2 )
        video.ctx.lineTo( cellOffset.x + tileOffset.x/2 , cellOffset.y                  )
        video.ctx.lineTo( cellOffset.x                  , cellOffset.y + tileOffset.y/2 )
        video.ctx.lineTo( cellOffset.x - tileOffset.x/2 , cellOffset.y                  )
        video.ctx.lineTo( cellOffset.x                  , cellOffset.y - tileOffset.y/2 )
        video.ctx.strokeStyle = `rgba(${rgb}, ${lineAlpha})`
        video.ctx.fillStyle = `rgba(${rgb}, ${lineAlpha / 2})`
        video.ctx.lineWidth = 2
        video.ctx.stroke()
        video.ctx.fill()
    }
}

module.exports = BattleUI
