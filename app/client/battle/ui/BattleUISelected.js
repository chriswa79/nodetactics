const BattleUI = require('./BattleUI')
const UnitHitbox = require('./UnitHitbox')

class BattleUISelected extends BattleUI {
    constructor(selectedUnit) {
        super()
        this.selectedUnit = selectedUnit
        this.unitSelectionMode = this.UnitSelectionMode.BIG
    }
    init() {
        super.init()
        if (this.unitSelectionMode !== this.UnitSelectionMode.NONE) {
            var filter = unit => unit !== this.selectedUnit // don't make a redundant hitbox for the currently selected unit
            var onClick = unit => this.view.setSelectedUnit(unit)
            this.createUnitHitboxes(filter, onClick, this.unitSelectionMode)
        }
    }
    createUnitHitboxes(filter, onClick, size, rgb) {
        _.forEach(this.model.units, unit => {
            if (!filter(unit)) { return }
            var unitOffset = this.view.cellCoordsToWorldPoint(unit.coords)
            this.appendChild(new UnitHitbox(unit, unitOffset, onClick, size, rgb))
        })
    }
    renderSquareUnit({ mousePos, mousePick, cell, cellOffset }) {
        var unit = this.unitCellIndexLookup[ cell.cellIndex ]
        if (unit) {
            var bitmap = unit.getBitmap('idle')
            if (unit === this.selectedUnit) {
                bitmap = bitmap.glow()
            }
            bitmap.blit(cellOffset.x, cellOffset.y)
        }
    }
    input_click(e) {
        this.view.setSelectedUnit() // reset selected unit
    }
}
BattleUISelected.prototype.UnitSelectionMode = { BIG: 'BIG', SMALL: 'SMALL', NONE: 'NONE' }

module.exports = BattleUISelected
