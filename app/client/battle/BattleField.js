const { DisplayNode } = require('../DisplayTree')
const { p } = require('../../common/geom')
var lerp = require('lerp')

const tileOffset = p(64, 32)

class BattleField extends DisplayNode {
    constructor(model, view) {
        super()
        this.model = model
        this.view = view
        this.offset = video.getCenter().subtract(p((this.model.field.grid.size - 1) * tileOffset.x / 2, 0))
        this.cameraTarget = undefined
        this.isDragging = false
    }
    setUI(ui) {
        if (this.ui) {
            this.removeChild(this.ui)
        }
        this.ui = ui
        this.ui.model = this.model
        this.ui.view = this.view
        this.ui.init()
        this.appendChild(ui)
    }
    centerOnUnit(unit, slow = false) {
        if (unit) {
            this.centerOnCoords(unit.coords, slow)
        }
    }
    centerOnCoords(coords, slow = false) {
        this.centerOnWorldPoint(this.view.cellCoordsToWorldPoint(coords), slow)
    }
    centerOnWorldPoint(point, slow = false) {
        this.cameraTarget = video.getCenter().subtract(point)
        if (!slow) {
            this.offset = this.cameraTarget
        }
    }
    renderMe({ mousePos, mousePick }, offset) {
        if (this.isDragging) {
            this.updateDrag(client.input)
        }
    }
    updateMe(dt) {
        if (this.cameraTarget) {
            this.offset.x = lerp(this.offset.x, this.cameraTarget.x, dt * 0.01);
            this.offset.y = lerp(this.offset.y, this.cameraTarget.y, dt * 0.01);
        }
    }

    updateDrag(input) { // called immediately before render for maximum responsiveness
        // continue dragging
        var deltaDrag = input.mouseDownPos.clone().subtract(input.mousePos)
        this.offset = this.dragOffsetStart.clone().subtract(deltaDrag)
    }

    pickMe() {
        return true // consume input!
    }
    input_dragstart(e) {
        this.dragOffsetStart = this.offset
        this.isDragging = true
        this.cameraTarget = undefined
    }
    input_dragend(e) {
        this.isDragging = false
    }

    realCellCoordsToRenderCellIndex(coords) {
        return this.model.field.grid.getCellIndex( p( Math.ceil(coords.x - 0.01), Math.floor(coords.y + 0.01) ) )
    }
    cellCoordsToScreenPoint(coords) {
        return this.view.cellCoordsToWorldPoint(coords).add(this.offset).round()
    }
    screenPointToWorldSpace(screenPoint) {
        var fieldPoint = screenPoint.clone().subtract(this.offset).round()
        var j = (fieldPoint.x / tileOffset.x) - (fieldPoint.y / tileOffset.y)
        var i = (fieldPoint.y / tileOffset.y) + (fieldPoint.x / tileOffset.x)
        return p(i, j)
    }
    screenPointToCellCoords(screenPoint) {
        return this.screenPointToWorldSpace(screenPoint).round()
    }
    getCellAtScreenPos(screenPos) {
        return this.model.field.grid.getCell(this.screenPointToCellCoords(screenPos))
    }
}

module.exports = BattleField
