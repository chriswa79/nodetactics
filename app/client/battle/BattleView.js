const { DisplayNode } = require('../DisplayTree')
const { p } = require('../../common/geom')
const Sky = require('./Sky')
const BattleUnitPanel = require('./BattleUnitPanel')
const BattleTurnClock = require('./BattleTurnClock')
const BattleField = require('./BattleField')
const BattleUI = require('./ui/BattleUI')

const tileOffset = p(64, 32)

class BattleView extends DisplayNode {
    constructor(model, decisionCallback) {
        super()
        this.decisionCallback = decisionCallback
        this.model = model
        this.queuedResultDocs = []
        this.currentlyPlayingResult = undefined
        this.sky = this.appendChild(new Sky())
        this.field = this.appendChild(new BattleField(this.model, this))
        this.unitPanel = this.appendChild(new BattleUnitPanel(this.model, this))
        this.turnClock = this.appendChild(new BattleTurnClock())
        this.tooltip = undefined
        this.lockUI()
    }

    setUI(ui) {
        this.field.setUI(ui)
    }
    lockUI() {
        this.setUI( new BattleUI() )
        this.unitPanel.selectUnit(undefined)
    }
    setSelectedUnit(unit) {
        unit = unit ? unit : this.model.getActiveUnit() // default to active unit
        this.selectedUnit = unit
        this.unitPanel.selectUnit(unit) // this will also call setUI
        this.field.centerOnUnit(unit, true) // true == slow pan
    }

    sendDecision(decisionDoc) {
        this.lockUI()
        this.decisionCallback(decisionDoc)
    }

    setTooltip(tooltip) {
        if (this.tooltip !== tooltip) {
            if (this.tooltip) {
                this.removeChild(this.tooltip)
            }
            this.tooltip = tooltip
            if (this.tooltip) {
                this.appendChild(this.tooltip)
            }
        }
        this._tooltipTimer = 3
    }
    updateMe(dt) {
        if (this.tooltip) {
            this._tooltipTimer -= 1
            if (this._tooltipTimer === 0) {
                this.removeChild(this.tooltip)
                this.tooltip = undefined
            }
        }
    }

    cellCoordsToWorldPoint(coords) {
        var x = (coords.x + coords.y) * tileOffset.x / 2
        var y = (coords.x - coords.y) * tileOffset.y / 2
        return p(x, y)
    }

}

module.exports = BattleView
