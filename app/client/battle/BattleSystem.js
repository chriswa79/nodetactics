const { DisplayNode } = require('../DisplayTree')
const Q = require('q')
const BattleModel = require('../../common/battle/model/BattleModel')
const BattleView  = require('./BattleView')
const BattleResultApplier = require('app/common/battle/BattleResultApplier')
const WatchingUI = require('./ui/watch')

class BattleSystem extends DisplayNode {
    constructor() {
        super()
        this.queuedResultDocs = []
        this.isPlayingResults = false
        this.stop()
    }
    syncRes(syncDoc) {
        syncDoc.battle && this.start(syncDoc.battle)
    }
    rpc_start(battleRes) {
        this.start(battleRes)
    }
    start(battleRes) {
        this.model = new BattleModel(battleRes.battleDoc)
        this.model.setMyTeamId(battleRes.myTeamId) // sent by server (unique per player,) but not saved to database
        this.view = this.appendChild(new BattleView(this.model, this.sendDecision.bind(this)))
        this.resultApplier = new BattleResultApplier(this.model)
        this.resetView()
    }
    resetView() {
        this.view.setSelectedUnit()
    }
    stop() {
        this.view && this.removeChild(this.view)
        this.model = undefined
        this.view  = undefined
    }

    sendDecision(decisionDoc) {
        client.network.send('battle.decision', decisionDoc)   // XXXXXX global
        this.view.lockUI() // wait for update
    }

    rpc_results(resultDocs) {
        this.queuedResultDocs.push(...resultDocs)
        this.playResults()
    }
    playResults(advanceDt = 0) {
        if (this.isPlayingResults) {
            return // already playing, just wait for current result to call playResults() when it's finished
        }

        // loop until we run out of resultDocs or we find one that creates a WatchingUI
        var nextResultDoc
        var watchUI
        while (true) {

            // done all queued results?
            if (!this.queuedResultDocs.length) {
                this.resetView()
                return
            }

            // get next resultDoc
            nextResultDoc = this.queuedResultDocs.shift()

            // when the watchUI is complete, apply the result and call playResults again (with the time left over)
            // n.b. when the battle is stopped, we removeChild the View, so the watchUI won't get updateMe's, and will never call onWatchingComplete
            var onWatchingComplete = timeLeftOver => {
                this.resultApplier.applyResult(nextResultDoc)
                this.isPlayingResults = false
                this.playResults(timeLeftOver) // play next result
            }

            // attempt to create a watchUI for it
            watchUI = WatchingUI.create(this.model, this.view, nextResultDoc, onWatchingComplete)

            // if this type of result has no WatchingUI, just apply the result immediately and continue to the next result
            if (!watchUI) {
                this.resultApplier.applyResult(nextResultDoc)
                continue
            }
            else {
                break
            }

        }

        // guard playResults from being run again until watching this result is complete
        this.isPlayingResults = true

        //
        this.view.setUI(watchUI)

        // finally, call update on watchUI
        // if this isn't the first result in the series, we will have had time left over from the last result;
        // advance the next result by the extra time from finishing up the last result
        watchUI.updateMe(advanceDt) // n.b. this may result in onWatchingComplete being called (therefore recursion)
    }

}

module.exports = BattleSystem
