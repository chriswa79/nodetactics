const { DisplayNode } = require('./DisplayTree')

class LoginSystem extends DisplayNode {
    constructor() {
        super()
    }
    syncReq(acc) {
        this.userKey = localStorage.userKey || this.generateKey()
        localStorage.userKey = this.userKey
        acc.userKey = this.userKey
    }
    generateKey() {
        var result = "";
        var charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var desiredLength = 32;
        for( var i = 0; i < desiredLength; i += 1 )
            result += charSet.charAt( Math.floor( Math.random() * charSet.length ) );
        return result;
    }
}

module.exports = LoginSystem
