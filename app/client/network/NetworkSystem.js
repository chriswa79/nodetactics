const { DisplayNode } = require('../DisplayTree')
const EventEmitter = require('events').EventEmitter
import WebsocketClient from './WebsocketClient'

function clog(msg) {
    //var consoleCSS = 'background: #222; color: #bada55';
    var consoleCSS = 'color: teal; font-weight: normal;';
    var tooLong = (msg.length > 200)
    tooLong && console.groupCollapsed('%c' + msg.substr(0, 200) + '...', consoleCSS)
    console.log('%c' + msg, consoleCSS)
    tooLong && console.groupEnd()
}

class NetworkSystem extends DisplayNode {
    constructor() {
        super()
        this.eventEmitter = new EventEmitter()
    }
    on(...args) {
        this.eventEmitter.on(...args)
    }
    emit(...args) {
        this.eventEmitter.emit(...args)
    }
    init(initData) {
        this.gui = client.gui

        this.isConnected = false

        this.gui.modal("Connecting to server", `Connecting to ${initData.websocketUrl}<BR>Please wait...`, [])
        //Client.gui(new Dialog({title: "Loading...", message: "Talking with the server, please wait..."}))

        this.websocket = new WebsocketClient({ url: initData.websocketUrl })

        this.websocket.on('connect', () => {
            clog("ws.connect")
            this.isConnected = true
            this.gui.modal("Syncronizing with server", `Connected.<BR>Please wait...`, [])
            var syncReqDoc = {}
            this.emit('broadcast', 'syncReq', syncReqDoc) // synchronous!
            this.send('network.syncReq', syncReqDoc) // ...and then wait for 'network.syncRes' message
        })

        this.websocket.on('message', msg => {
            clog("ws.recv " + JSON.stringify(msg))
            this.emit('message', msg)
        })

        this.websocket.on('error', err => {
            if (this.isConnected) {
                console.error(err)
            }
        })

        this.websocket.on('disconnect', () => {
            if (this.isConnected) {
                clog("ws.disconnect")
                this.emit('broadcast', 'disconnect')
                //this.gui.modal("Lost connection to server", `Re-connecting to ${initData.websocketUrl}<BR>Please wait...`, [])
                this.gui.modal("Debug Mode", `Lost connection to server!<BR>Refresh to re-connect!`, [])
            }
            this.isConnected = false
            //setTimeout(() => this.websocket.connect(), 150)
        })

    }
    send(eventName, ...eventArgs) {
        if (this.isConnected) {
            clog("ws.send " + JSON.stringify([eventName, ...eventArgs]))
            this.websocket.send([eventName, ...eventArgs])
        }
    }
    rpc_syncRes(syncDoc) {
        this.gui.clear()
        // forward the sync response document to any and all systems which need to sync
        this.emit('broadcast', 'syncRes', syncDoc)
    }
}

module.exports = NetworkSystem
