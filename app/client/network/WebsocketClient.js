const EventEmitter = require('events')

class WebsocketClient extends EventEmitter {
    constructor(options) {
        super()
        this.url = options.url
        this.connect()
    }
    connect() {
        var WebSocket = window.WebSocket || window.MozWebSocket
        this.connection = new WebSocket(this.url)

        this.connection.onopen = () => {
            this.emit('connect')
        }

        this.connection.onerror = error => {
            this.emit("error", error)
        }

        this.connection.onclose = event => {
            this.emit('disconnect', event)
        }

        this.connection.onmessage = message => {
            try {
                var payload = JSON.parse(message.data)
            } catch (e) {
                console.log("ws: error! JSON parse failed: ", message.data)
                return
            }
            this.emit('message', payload)
        }
    }
    disconnect() {
        this.connection.close()
    }
    send(obj) {
        this.connection.send(JSON.stringify(obj))
    }
}

module.exports = WebsocketClient
