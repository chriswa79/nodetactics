var p = require('../common/geom').p

const dragThreshold = 4

class InputSystem {
    constructor(root, canvas) {
        this.root = root

        this.mousePos  = p(0, 0)
        this.mouseDown = false
        this.mouseDownPos = undefined

        this.mouseDownNode = undefined
        this.dragNode = undefined

        var mousePosUpdate = e => this.mousePos = p(e.pageX, e.pageY)

        document.onmousemove = e => {
            mousePosUpdate(e)
        }
        // document.onclick = e => {
        //     mousePosUpdate(e)
        //     this.fire('click')
        // }
        canvas.onmousedown = e => {
            mousePosUpdate(e)
            this.mouseDownPos = this.mousePos
            this.mouseDown = true
            this.startEvent('mousedown', {})
        }
        document.onmouseup = e => {
            mousePosUpdate(e)
            this.mouseDown = false
            this.startEvent('mouseup', {})
        }
        document.onkeydown = e => {
            this.startEvent('keydown', { key: e.keyCode })
        }
        document.onkeyup = e => {
            this.startEvent('keyup', { key: e.keyCode })
        }
        document.onkeypress = e => {
            this.startEvent('keypress', { key: e.keyCode })
        }
    }

    startEvent(eventName, eventData) {
        var targetNode = this.root.pickFirst(this.mousePos)

        if (eventName === 'mousedown') {
            this.mouseDownNode = targetNode // remember this
        }

        this.trickleAndBubble(targetNode, eventName, eventData)

        if (eventName === 'mouseup') {
            // if we were dragging, fire a 'dragend' event
            if (this.dragNode) {
                this.trickleAndBubble(this.dragNode, 'dragend', {})
                this.dragNode = undefined
            }
            else {
                // if we were *not* dragging, and mouseup and mousedown happened on the same node, then fire a 'click'
                if (targetNode === this.mouseDownNode) {
                    this.trickleAndBubble(targetNode, 'click', eventData)
                }
            }
            this.mouseDownNode = undefined
        }
    }

    trickleAndBubble(targetNode, eventName, eventData) {
        //console.log("trickleAndBubble " + eventName, targetNode, eventData)

        if (!targetNode) { return }
        var ancestors = targetNode.getAncestors()
        ancestors.unshift(targetNode)

        eventData.targetNode = targetNode

        eventData.input = this

        var stopPropagation = false
        eventData.stopPropagation = () => { stopPropagation = true }

        // capture/trickle phase
        var methodName = 'input_' + eventName + '_capture'
        ancestors.reverse()
        ancestors.forEach(node => {
            if (!stopPropagation && node[methodName]) {
                //console.log("... trickle", node)
                node[methodName](eventData)
            }
        })

        // bubble phase
        methodName = 'input_' + eventName
        ancestors.reverse()
        ancestors.forEach(node => {
            if (!stopPropagation && node[methodName]) {
                //console.log("... bubble", node)
                node[methodName](eventData)
            }
        })

    }

    update() {
        // if the mouse is down (starting from a node) and we aren't yet dragging...
        if (this.mouseDownNode && !this.dragNode) {
            // check if the mouse has moved past the dragThreshold
            if (this.mousePos.clone().subtract(this.mouseDownPos).isMagnitudeGreaterThan(dragThreshold)) {
                //console.log('dragstart!')
                this.dragNode = this.mouseDownNode
                this.trickleAndBubble(this.dragNode, 'dragstart', {})
            }
        }
    }

    getRenderPayload() {

        var mousePos = this.mousePos
        var mousePick = this.dragNode ? this.dragNode : this.root.pickFirst(mousePos)

        return { mousePos, mousePick }
    }


}

module.exports = InputSystem
