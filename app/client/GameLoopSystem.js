const EventEmitter = require('events').EventEmitter

class GameLoopSystem extends EventEmitter {
    constructor() {
        super()
        this.isRunning = false
        this.simTime = _.now()
        this.SIM_SPEED           = 1.0
        this.SIM_STEP_MIN        = 1000 * 1/60 // for stability, simulation delta times will never be smaller than this
        this.SIM_STEP_MAX        = 1000 * 1/30 // for stability, simulation delta times will never be greater than this
        this.SIM_STEP_HARD_LIMIT = 1000 * 3/30 // for playability, each render frame will never advance simulation time more than this (also avoiding the game gameloop "death spiral")
    }

    start() {
        if (this.isRunning) { return }
        this.isRunning = true
        this.simTime = _.now()
        requestAnimationFrame(this.onAnimationFrame.bind(this))
        this.emit('unpause') // e.g. Audio.unpauseMusic re-render to clear paused screen
    }

    stop() {
        if (!this.isRunning) { return }
        this.isRunning = false
        this.emit('pause') // e.g. Audio.pauseMusic() Video.drawPausedScreen()
    }

    step(dt) {
        this.stop()
        this.update(dt)
        this.simTime += dt
        this.render()
    }

    onAnimationFrame() {
        if (!this.isRunning) { return }

        var dt = (_.now() - this.simTime) * this.SIM_SPEED

        // if more than the minimum time has passed, update then render
        if (dt > this.SIM_STEP_MIN) {

            // if more than the hard limit has passed, allow the game to slow down to avoid the spiral of death
            if (dt > this.SIM_STEP_HARD_LIMIT && this.SIM_SPEED === 1.0) {
                //console.log('App: SLOWDOWN! ' + (dt - this.SIM_STEP_HARD_LIMIT) + 'ms abandoned!')
                this.simTime += dt - this.SIM_STEP_HARD_LIMIT
                dt = this.SIM_STEP_HARD_LIMIT
            }

            // figure out how many updates to run (we need more than 1 if more than the maximum time has passed)
            var requiredSteps = Math.ceil( dt / this.SIM_STEP_MAX )
            var stepDt        = dt / requiredSteps
            //if (requiredSteps > 1) { console.log('rendering ' + requiredSteps + ' simulation steps this frame because dt > SIM_STEP_MAX') }
            for ( var i = 0; i < requiredSteps; i++ ) {
                this.emit('update', stepDt)
                this.simTime += stepDt / this.SIM_SPEED
            }

            // render!
            this.emit('render')
        }

        // gameloop!
        requestAnimationFrame( this.onAnimationFrame.bind(this) )
    }
}

module.exports = GameLoopSystem

