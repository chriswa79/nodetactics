const { p } = require('app/common/geom')
const { DisplayNode } = require('app/client/DisplayTree')
const GuiTextFlow = require('./GuiTextFlow')

// you can create multiple tooltips for free; they won't flow until they are added to the DisplayTree

class GuiTooltip extends DisplayNode {
    constructor(maxWidth, markup) {
        super()
        this.maxWidth = maxWidth
        this.markup = markup
        this.flow = undefined
    }
    renderMe({ mousePos, mousePick }, offset) {
        offset.add(p(11, 11))
        if (!this.flow) {
            this.flow = new GuiTextFlow( this.maxWidth ).markup( this.markup )
        }
        video.fillRoundedRect(offset, this.flow.size, 11, 11, '#cccccc')
        video.fillRoundedRect(offset, this.flow.size, 10, 10, '#132020')
        this.flow.render(offset)
    }
}

module.exports = GuiTooltip
