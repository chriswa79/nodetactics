const { p } = require('app/common/geom')
const { DisplayNode } = require('app/client/DisplayTree')

/*

# Usage

class MyGuiWidget extends DisplayNode {
    constructor() {
        super()
        this.maxWidth = 200
        this.contentFunction = flow => {
            flow.header("Header", '#99ccff')
            flow.floatRight("Light", 'white')
            flow.text("Left", 'white')
            flow.newline()
            flow.text("Cures", 'white')
            flow.text("15", 'orange', true)
            flow.text("hunger", 'white')
        }
    }
    renderMe({ mousePos, mousePick }, offset) {
        if (!this.flow) {
            this.flow = new GuiTextFlow( this.maxWidth ).executeContentFunction( this.contentFunction )
        }
        video.fillRoundedRect(offset, this.flow.size, 10, 10, '#132020')
        this.flow.render(offset)
    }
}

class MyGuiWidget2 extends DisplayNode {
    constructor(markup) {
        super()
        this.maxWidth = 200
        this.markup = markup
    }
    renderMe({ mousePos, mousePick }, offset) {
        if (!this.flow) {
            this.flow = new GuiTextFlow( this.maxWidth ).markup( this.markup )
        }
        video.fillRoundedRect(offset, this.flow.size, 10, 10, '#132020')
        this.flow.render(offset)
    }
}


 */
class GuiTextLine {
    constructor(maxWidth, paddingTop = 0, paddingBottom = 0) {
        this.maxWidth = maxWidth
        this.height = 0
        this.paddingTop = paddingTop
        this.paddingBottom = paddingBottom
        this.cursorLeft = 0
        this.spans = []
    }
    floatRight(textData) {
        var {text, colour, bold, pixelHeight, fontFamily} = textData

        var font = (bold ? 'bold ' : '') + pixelHeight + 'px ' + fontFamily
        video.ctx.font = font
        video.ctx.fillStyle = colour

        var textWidth = video.ctx.measureText(text).width

        this.spans.push({ text, x: this.maxWidth - textWidth, font, colour })

        this.maxWidth -= textWidth + 10
    }
    text(textData) {
        var {text, colour, bold, pixelHeight, fontFamily} = textData

        var font = (bold ? 'bold ' : '') + pixelHeight + 'px ' + fontFamily
        video.ctx.font = font
        video.ctx.fillStyle = colour

        var measure = (string) => video.ctx.measureText(string).width

        // if this line already has text in it, pad it with a space
        var allText = text
        if (this.cursorLeft > 0) {
            allText = ' ' + allText
        }

        var span = { text: '', x: this.cursorLeft, font, colour }

        // see if it will all fit on the current line
        var remainingWidth = this.maxWidth - this.cursorLeft
        var fullWidth = measure(allText)
        if (fullWidth <= remainingWidth) {
            span.text = allText
            this.spans.push(span)
            this.height = Math.max(this.height, pixelHeight)
            this.cursorLeft += fullWidth
            return undefined
        }

        // next, try to add one word at a time
        var words = text.split(/\s+/).map(word => ' ' + word)
        if (this.cursorLeft <= 0) {
            words[0] = words[0].substr(1) // remove first space
        }
        while (words.length) {
            var wordWidth = measure(words[0])
            if (wordWidth > remainingWidth) {
                break
            }
            span.text += words[0]
            this.cursorLeft += wordWidth
            remainingWidth -= wordWidth
            words.shift()
        }

        // if words were added to this line, return the remaining words
        if (span.text.length) {
            this.spans.push(span)
            this.height = Math.max(this.height, pixelHeight)
            words[0] = words[0].substr(1) // remove first space
            return words.join('')
        }

        // if no words could be added to this line, and the line is empty, start adding characters (force-break words!)
        if (!this.spans.length) {
            while (true) {
                var letter = text.substr(0, 1)
                var letterWidth = measure(letter)
                if (letterWidth > remainingWidth) {
                    this.spans.push(span)
                    this.height = Math.max(this.height, pixelHeight)
                    break
                }
                span.text += letter
                this.cursorLeft += letterWidth
                remainingWidth -= letterWidth
                text = text.substr(1)
            }
        }
        return text
    }
    render(offsetX, offsetY) {
        _.each(this.spans, span => {
            video.ctx.font = span.font
            video.ctx.fillStyle = span.colour
            video.ctx.fillText(span.text, offsetX + span.x, offsetY)
        })
    }
}

class GuiTextFlow {
    constructor(maxWidth) {
        this.maxWidth = maxWidth
        this.lines = []
        this.newline(0)
        this.size = p(0, 0)
    }
    _calculateSize() {
        var width = _.maxBy(this.lines, 'cursorLeft').cursorLeft
        var height = _.reduce(this.lines, (acc, line) => acc + line.height + line.paddingTop + line.paddingBottom, 0)
        this.size = p(width, height)
        return this.size
    }

    _ensureNewLine() {
        if (this._currentLine().length) { this.newline(0) }
    }
    _currentLine() {
        return this.lines.slice(-1)[0] // last array element
    }
    _writeText(text, colour = 'white', bold = false, pixelHeight = 12, fontFamily = 'Verdana') {
        var textData = { text, colour, bold, pixelHeight, fontFamily }
        while (true) {
            textData.text = this._currentLine().text(textData)
            if (!textData.text) { break }
            this.newline(3)
        }
    }
    newline(paddingTop = 5) {
        this.lines.push(new GuiTextLine(this.maxWidth, paddingTop))
    }

    header(text, colour) {
        this._ensureNewLine()
        this._currentLine().cursorLeft = -1
        this._writeText(text, colour, true, 20)
        this._currentLine().paddingTop = -5
        this._currentLine().paddingBottom = 2
        this.newline(6)
    }
    text(text, colour, bold = false) {
        this._writeText(text, colour, bold, 12)
    }
    floatRight(text, colour, bold = false, pixelHeight = 12, fontFamily = 'Verdana') {
        this._currentLine().floatRight({ text, colour, bold, pixelHeight, fontFamily })
    }

    markup(markup) {
        if (markup) {
            var styleStack = [{colour: 'white'}]
            while (markup.length) {
                var nextTagStart = markup.indexOf('<')
                if (nextTagStart === 0) {
                    var nextTagEnd = markup.indexOf('>')
                    var tag = markup.substr(1, nextTagEnd - 1)
                    markup = markup.substr(nextTagEnd + 1)
                    if (tag.substr(0, 1) === '/') {
                        styleStack.pop()
                        continue
                    }
                    if (tag === 'BR') {
                        this.newline()
                        continue
                    }
                    var newStyle = _.clone(styleStack.slice(-1)[0]) // last array element
                    if (_.indexOf(['b', 'H', 'R'], tag) !== -1) {
                        newStyle[tag] = true
                        if (tag === 'H') { newStyle.colour = '#ffee99' }
                        styleStack.push(newStyle)
                    }
                    else {
                        newStyle.colour = tag
                        styleStack.push(newStyle)
                    }
                }
                else {
                    var textEnd = nextTagStart > 0 ? nextTagStart : markup.length
                    var text = markup.substr(0, textEnd)
                    text = _.trim(text)

                    var currentStyle = styleStack.slice(-1)[0] // last array element
                    if (currentStyle['H']) {
                        this.header(text, currentStyle.colour)
                    }
                    else if (currentStyle['R']) {
                        this.floatRight(text, currentStyle.colour, currentStyle['b'])
                    }
                    else {
                        this.text(text, currentStyle.colour, currentStyle['b'])
                    }

                    markup = markup.substr(textEnd)
                }
            }
        }
        this._calculateSize()
        return this
    }
    executeContentFunction(contentFunction) {
        contentFunction(this)
        this._calculateSize()
        return this
    }

    render(offset) {
        var myOffset = offset.clone()
        _.each(this.lines, line => {
            myOffset.y += line.height + line.paddingTop
            line.render(myOffset.x, myOffset.y)
            myOffset.y += line.paddingBottom
        })
    }
}

module.exports = GuiTextFlow
