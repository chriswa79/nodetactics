const { DisplayNode } = require('../DisplayTree')
const Dialog = require('./Dialog')

class GuiSystem extends DisplayNode {
    constructor() {
        super()
        this.dialog = undefined
    }
    modal(title, message, buttons) {
        this.clear()

        if (message === undefined) {
            message = title
            title = "Untitled Dialog"
        }
        if (buttons === undefined) {
            buttons = [ { label: "Ok" } ]
        }

        this.dialog = this.appendChild(new Dialog({ title, message, buttons }))
    }
    clear() {
        if (this.dialog) {
            this.dialog.remove()
            this.dialog = undefined
        }
    }
    pickMe() {
        return !!this.dialog
    }
}

module.exports = GuiSystem
