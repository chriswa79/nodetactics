const { DisplayNode } = require('../DisplayTree')
const { p, rect } = require('../../common/geom')
const GuiTextFlow = require('./GuiTextFlow')

class Button extends DisplayNode {
    constructor(options, offset) {
        super()
        this.options = options
        this.offset = offset.clone()
        this.rect = rect(p(options.textWidth + 35, 30))
    }
    renderMe({ mousePos, mousePick }, offset) {
        var isHovered = mousePick === this

        var colour = isHovered ? '#0ca0c0' : '#0A84AE'
        video.fillRoundedRect( this.rect.topLeft.plus(offset), this.rect.size, 17, 0, colour )
        // label
        video.ctx.fillStyle = 'white'
        video.ctx.fillText(this.options.label, offset.x + this.rect.topLeft.x + 17, offset.y + this.rect.topLeft.y + 20)
    }
    input_click() {
        this.options.onClick && this.options.onClick()
        global.client.gui.clear() // XXXXXXXXX global
    }
}

class Dialog extends DisplayNode {
    constructor(options) {
        super()
        this.offset    = video.getCenter()
        this.title     = options.title   || "Untitled Dialog"
        this.bodyText  = options.message || ""
        this.buttons   = options.buttons || []
        this.titleFont = "bold 15px Verdana"
        this.bodyFlow  = undefined
        this.init()
    }
    init() {
        this.bodyFlow = new GuiTextFlow( 200 ).markup( this.bodyText )
        video.ctx.font = this.titleFont
        this.titleWidth = video.ctx.measureText(this.title).width
        video.ctx.font = "12px Verdana" // button font

        var origin = video.getCenter()
        var buttonOrigin = p(150, this.bodyFlow.size.y + 15)

        this.buttons = this.buttons.map(button => {
            button.textWidth = video.ctx.measureText(button.label).width
            buttonOrigin.x -= button.textWidth + 40
            return this.appendChild(new Button(button, buttonOrigin))
        })
    }
    renderMe({ mousePos, mousePick }, offset) {

        // darken entire canvas
        video.ctx.rect(0, 0, video.canvas.width, video.canvas.height)
        video.fill('rgba(0, 0, 0, 0.6)')

        var hasButtons = this.buttons.length > 0
        var buttonPadding = hasButtons ? 35 : 0

        var bHeight = this.bodyFlow.size.y

        // draw black border box around everything
        video.fillRoundedRect( p( -128, -28 ).add(offset), p( 256, 21 + bHeight + buttonPadding ), 25, 25, 'black' )
        // draw box around entire menu
        video.fillRoundedRect( p( -130, -30 ).add(offset), p( 260, 25 + bHeight + buttonPadding ), 20, 20, '#004080' )
        // draw box around header
        video.fillRoundedRect( p( -128, -28 ).add(offset), p( 256,                           -4 ), 17, 17, '#0A84AE' )
        // draw box around body
        video.fillRoundedRect( p( -128,   7 ).add(offset), p( 256,                 bHeight - 14 ), 17, 17, '#0066cc' )

        // write title text
        video.ctx.fillStyle = 'white'
        video.ctx.font = this.titleFont
        video.ctx.fillText(this.title, offset.x - this.titleWidth / 2, offset.y - 25)
        // write body text
        video.ctx.fillStyle = 'white'
        this.bodyFlow.render(p(offset.x - 132, offset.y))
    }
}

module.exports = Dialog
