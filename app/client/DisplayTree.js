const assert = require('assert')
const { p } = require('../common/geom')

class TreeNode {
    constructor() {
        this.children = []
    }
    remove() {
        if (this.parent) {
            this.parent.removeChild(this)
        }
    }
    removeChild(child) {
        _.pull(this.children, child)
        child.root = undefined
    }
    removeAllChildren() {
        this.children = [] // n.b. does not reset child.root or clear references from root.ids
    }
    _prepareToAddChild(child) {
        child.remove()
        child.parent = this
        child.root = this.root
    }
    appendChild(child) {
        this._prepareToAddChild(child)
        this.children.push(child)
        child.root = this.root // n.b. this should also set child.children.root, etc
        return child
    }
    get firstChild() {
        return this.children[0]
    }
    _getChildIndex(child) {
        return _.findIndex(this.children, child)
    }
    insertBefore(child, reference) {
        this._prepareToAddChild(child)
        var referenceIndex = this._getChildIndex(reference)
        this.children.splice(referenceIndex, 0, child)
        return child
    }
    appendTo(newParent) {
        newParent.appendChild(this)
        return this
    }
    getAncestors() {
        var ancestors = []
        var cursor = this
        while (cursor.parent) {
            cursor = cursor.parent
            ancestors.push(cursor)
        }
        return ancestors
    }

    forEach(fn, acc) {
        fn(this, acc)
        for ( var child of this.children ) {
            child.forEach(fn, acc)
        }
    }
    forEachImmediateChildren(fn) {
        for ( var child of this.children ) {
            fn(child)
        }
    }
    findFirst(fn) {
        var myResult = fn(this)
        if (myResult) { return myResult }
        for ( var child of this.children ) {
            var childResult = child.findFirst(fn)
            if (childResult) { return childResult }
        }
        return undefined
    }
}

class DisplayNode extends TreeNode {
    constructor() {
        super()
        this.offset = p(0, 0)
    }
    pickMe(point) {
        return this.rect ? this.rect.containsPoint(point) : false
    }
    pickFirst(point) {
        if (!point) {
            point = p(0, 0)
        }
        point = point.clone().subtract(this.offset)
        for ( var i = this.children.length - 1; i >= 0; i -= 1 ) { // reversed to match up with render order
            var childResult = this.children[i].pickFirst(point)
            if (childResult) { return childResult }
        }
        if (this.pickMe(point)) {
            return this
        }
        return undefined
    }
    // pickFirst(point, prop) {
    //     if (!point) {
    //         point = p(0, 0)
    //     }
    //     point = point.clone().subtract(this.offset)
    //     for ( var i = this.children.length - 1; i >= 0; i -= 1 ) { // reversed to match up with render order
    //         var childResult = this.children[i].pickFirst(point, prop)
    //         if (childResult) { return childResult }
    //     }
    //     if ((!prop || this[prop]) && this.pickMe(point)) {
    //         return this
    //     }
    //     return undefined
    // }
    // pickCallback(point, callback) {
    //     if (this.offset) {
    //         point = point.clone().subtract(this.offset)
    //     }
    //     for ( var i = this.children.length - 1; i >= 0; i -= 1 ) { // reversed to match up with render order
    //         var childResult = this.children[i].pickCallback(point, callback)
    //         if (childResult) { return childResult }
    //     }
    //     return this.pickMe(point) ? callback(this) : undefined
    // }
    // pickFirst(point) {
    //     if (this.offset) {
    //         point = point.clone().subtract(this.offset)
    //     }
    //     for ( var i = this.children.length - 1; i >= 0; i -= 1 ) { // reversed to match up with render order
    //         var childResult = this.children[i].pickFirst(point)
    //         if (childResult) { return childResult }
    //     }
    //     return this.pickMe(point) ? this : undefined
    // }
    // pickAll(point) {
    //     var results = []
    //     if (this.offset) {
    //         point = point.clone().subtract(this.offset)
    //     }
    //     for ( var i = this.children.length - 1; i >= 0; i -= 1 ) { // reversed to match up with render order
    //         var childResults = this.children[i].pickAll(point)
    //         if (childResults) { results.push(...childResults) }
    //     }
    //     if (this.pickMe(point)) {
    //         results.push(this)
    //     }
    //     return results
    // }
    renderMe(payload, offset) {
    }
    render(payload, parentOffset) {
        if (!parentOffset) { parentOffset = p(0, 0) }
        var offset = parentOffset
        if (this.offset) {
            offset = parentOffset.plus(this.offset).round()
        }

        this.renderMe(payload, offset)

        // if renderMe adjusted this.offset, get the new offset for children below
        if (this.offset) {
            offset = parentOffset.plus(this.offset).round()
        }

        // render children
        for ( var child of this.children ) {
            child.render(payload, offset)
        }
    }
    updateMe(payload) {
    }
    update(payload) {
        this.updateMe(payload)
        for ( var child of this.children ) {
            child.update(payload)
        }
    }
}

module.exports = {
    newRoot: () => new DisplayNode(),
    DisplayNode,
}
