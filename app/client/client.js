import 'babel-polyfill'

global.DEBUG = true

global._ = require('lodash')

const VideoSystem    = require('./video/VideoSystem')
const GameLoopSystem = require('./GameLoopSystem')
const InputSystem    = require('./InputSystem')
const NetworkSystem  = require('./network/NetworkSystem')
const LoginSystem    = require('./LoginSystem')
const BattleSystem   = require('./battle/BattleSystem')
const GuiSystem      = require('./gui/GuiSystem')
const HudSystem      = require('./HudSystem')

const DisplayTree = require('./DisplayTree')

const initData = {
    websocketUrl: 'ws://' + location.host,
}

new class {
    constructor() {

        this.root = DisplayTree.newRoot()

        this.video    = new VideoSystem()
        this.gameloop = new GameLoopSystem()
        this.input    = new InputSystem(this.root, this.video.canvas) // set up input system to fire events in node tree

        this.network  = new NetworkSystem() .appendTo(this.root)
        this.login    = new LoginSystem()   .appendTo(this.root)
        this.battle   = new BattleSystem()  .appendTo(this.root)
        this.gui     = new GuiSystem()     .appendTo(this.root)
        this.hud      = new HudSystem()     .appendTo(this.root)

        // video events
        this.video.on('loaded', () => this.gameloop.start() )

        // gameloop events
        this.gameloop.on('update', dt => this.root.update(dt) )
        this.gameloop.on('render', () => {
            this.input.update()
            var renderPayload = this.input.getRenderPayload()
            this.root.render( renderPayload )
        } )

        // network events
        this.network.on('broadcast', (methodName, ...args) => this.broadcastToTopLevelChildren(methodName, ...args))
        this.network.on('message', ([eventName, ...args]) => {
            var [systemId, methodName] = eventName.split('.')
            this[systemId] && this[systemId]['rpc_' + methodName] && this[systemId]['rpc_' + methodName](...args)
        })

        // globals
        global.toast = (...args) => this.hud.addToastMessage(...args)
        global.client = this
        global.video = this.video

        // init
        this.broadcastToTopLevelChildren('init', initData)
        this.video.init()

    }
    broadcastToTopLevelChildren(methodName, ...methodArgs)  {
        this.root.forEachImmediateChildren( child =>
            child[methodName] && child[methodName](...methodArgs)
        )
    }
    modal(...args) {
        this.gui.modal(...args)
    }
}

