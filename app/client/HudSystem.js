const { DisplayNode } = require('./DisplayTree')

const messageSolidDuration = 2500
const messageFadeDuration  = 500

class HudSystem extends DisplayNode {
    constructor() {
        super()
        this.toastMessages = []
    }
    update(dt) {
        this.toastMessages.forEach(toast => toast.age += dt)
        this.toastMessages = this.toastMessages.filter(toast => toast.age < messageSolidDuration + messageFadeDuration )
    }
    render() {
        video.ctx.font = '12px Verdana'
        this.toastMessages.forEach((toast, index) => {
            var alpha = toast.age < messageSolidDuration ? 1.0 : 1.0 - (toast.age - messageSolidDuration) / messageFadeDuration
            video.ctx.lineWidth = 5
            video.ctx.lineJoin = 'round'
            video.ctx.strokeStyle = `rgba(0, 0, 0, ${alpha})`
            video.ctx.strokeText(toast.message, 2, 16 + index * 18)
            video.ctx.fillStyle = `rgba(${toast.rgb}, ${alpha})`
            video.ctx.fillText(toast.message, 2, 16 + index * 18)
        })
    }
    addToastMessage(message, rgb) {
        console.log(`Toast: ${message}`)
        if (rgb === undefined) { rgb = '255, 255, 255' }
        this.toastMessages.unshift({ age: 0, message, rgb })
    }
    rpc_toast(message) {
        this.addToastMessage(message)
    }

}

module.exports = HudSystem
