'use strict';

var gulp          = require('gulp');                    // Base gulp package
var babelify      = require('babelify');                // Used to convert ES6 & JSX to ES5
var browserify    = require('browserify');              // Providers "require" support, CommonJS
var notify        = require('gulp-notify');             // Provides notification to both the console and Growel
var rename        = require('gulp-rename');             // Rename sources
var sourcemaps    = require('gulp-sourcemaps');         // Provide external sourcemap files
//var livereload    = require('gulp-livereload');         // Livereload support for the browser
var gutil         = require('gulp-util');               // Provides gulp utilities, including logging and beep
var chalk         = require('chalk');                   // Allows for coloring for logging
var source        = require('vinyl-source-stream');     // Vinyl stream support
var buffer        = require('vinyl-buffer');            // Vinyl stream support
var watchify      = require('watchify');                // Watchify for source changes
var merge         = require('utils-merge');             // Object merge tool
var duration      = require('gulp-duration');           // Time aspects of your gulp process
var nodemon       = require('gulp-nodemon');
var es            = require('event-stream');

// Configuration for Gulp
var CONFIG = {
    server: {
        main: 'node_modules/app/server/server.js',
        nodemon: {
            verbose: true,
            script: 'node_modules/app/server/server.js',
            ext: 'js html',
            watch: ['node_modules/app/**/*'],
            ignore: [
                'www/dist/',
                'db/',
                'node_modules/app/client/',
            ],
            ignoreRoot: [], // don't force-ignore node_modules!
            env: {'NODE_ENV': 'development'},
        },
    },
    client: {
        sources: ['./node_modules/app/client/client.js'], // , './node_modules/app/client/standalone.js'],
        buildDir: './www/dist/',
        babel: {
            presets: ['es2015-node5' /* , 'stage-0', 'react'*/],
            compact: true,
            comments: false,
            minified: true,
        },
    },
};


gulp.task('default', ['client', 'server']);

gulp.task('server', function () {
    nodemon(CONFIG.server.nodemon).on('start', function () {
        console.log('nodemon start!')
        //livereload()
    })
});

// Gulp task for build
gulp.task('client', done => {
    //livereload.listen(); // Start livereload server

    var config = CONFIG.client

    const tasks = config.sources.map(entry => {
        var outFile = entry.replace('node_modules/app/client/', '')

        var opts = Object.assign({entries: entry, debug: true}, watchify.args)
        var b = watchify(browserify(opts), {poll: true}).transform(babelify, config.babel)

        function bundle() {
            var bundleTimer = duration('Javascript bundle time');
            //notify().write({ message: 'Bundle started...' })
            return b.bundle()
            // log errors if they happen
                .on('error', gutil.log.bind(gutil, 'Browserify Error'))
                .pipe(source(outFile))
                // optional, remove if you don't need to buffer file contents
                .pipe(buffer())
                .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
                // Add transformation tasks to the pipeline here.
                .pipe(sourcemaps.write('./')) // writes .map file
                .pipe(gulp.dest(config.buildDir))
                .pipe(bundleTimer)
        }

        b.on('update', bundle) // on any dep update, runs the bundler
        b.on('log', gutil.log)
        b.on('log', () => notify().write({message: outFile}))
        return bundle()
    })
    es.merge(tasks).on('end', done);
});

// Error reporting function
function mapError(err) {
    if (err.fileName) {
        // Regular error
        notify().write({title: 'Regular error', message: err.description})
        gutil.log(chalk.red(err.name)
            + ': ' + chalk.yellow(err.fileName.replace(__dirname + '/node_modules/app/', ''))
            + ': ' + 'Line ' + chalk.magenta(err.lineNumber)
            + ' & ' + 'Column ' + chalk.magenta(err.columnNumber || err.column)
            + ': ' + chalk.blue(err.description));
    }
    else {
        // Browserify error..
        notify().write({title: 'Browserify error', message: err.message.replace(/^\S+\/nodetactics\/\S+\s/, '')})
        gutil.log(chalk.red(err.name)
            + ': '
            + chalk.yellow(err.message));
    }
}
